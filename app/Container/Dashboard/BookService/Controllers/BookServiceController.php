<?php

namespace App\Container\Dashboard\BookService\Controllers;

use App\Container\Dashboard\BookService\Models\BookService;
use App\Container\Dashboard\BookService\Models\Service;
use App\Http\Controllers\Controller;

class BookServiceController extends Controller
{
    public function index()
    {
        $data['services'] = Service::first();
        return redirect(route('dashboard.service.index.spec',['id'=>$data['services']->id]));
    }
    public function spec($id) {
        $data['services'] = Service::all();
        $data['id'] = $id;
        $data['books'] =BookService::where('services','like', '%['.$id.'%')->orWhere('services','like', '%, '.$id.',%')->orWhere('services','like', '%, '.$id.']%')->paginate();
        if($id==0) {
            $data['books'] = BookService::paginate();
        }
        return view('dashboard.bookService.index',$data);
    }
    public function detail($id) {
        $data['book'] = BookService::find($id);
        $data['services'] = Service::all();
        return view('dashboard.bookService.detail',$data);
    }
}
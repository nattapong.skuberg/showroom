<?php

namespace App\Container\Home\TestDrive\Controllers;

use App\Container\Dashboard\Series\Models\Series;
use App\Container\Dashboard\TestDrive\Models\Province;
use App\Container\Dashboard\TestDrive\Models\TestDrive;
use App\Http\Controllers\Controller;
use App\Services\MailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestDriveController extends Controller
{
    public function index()
    {
        $data['series'] = Series::orderByDesc('id')->get();
        $data['provinces'] = Province::all();
        $data['branch'] = ['จรัญสนิทวงศ์','รามอินทรา'];
        return view('home.testDrive.index',$data);
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'series' => 'required',
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'branch' => 'required',
            'optradio' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $test = new TestDrive();
        $test->series_id = $request->input('series');
        $test->first_name = $request->input('name');
        $test->last_name = $request->input('lastname');
        $test->email = $request->input('email');
        $test->phone = $request->input('phone');
        $test->address = $request->input('branch');
        $test->province_id = 0;
        $test->district_id = 0;
        $test->sub_district_id = 0;
        $test->postcode = 0;
        $test->status = $request->input('optradio');
        $test->drive_time = now();
        $test->save();
        $email = new MailService();
        $contact = '';
        $detail = 'New test drive request <br> contact user at tel:'.$test->phone.'<br>email: '.$test->email.'<br>You can see detail at '.route('dashboard.testDrive.detail',['id'=>$test->id]);
        $email->sendMailToAdmin($test->first_name.' '.$test->last_name, $detail, now(), 'New test drive request from '.$test->first_name.' '.$test->last_name.' ('.$request->input('branch').')', $contact);

        $data['message'] = "การจองเสร็จสิ้น";
        return redirect('/')->with($data);
    }
    public function contactTestDrive(Request $request) {
        try {
            $email = new MailService();
            $series = Series::find($request->input('model'));
            $contact = 'email : '.$request->input('email').'<br>Tel : '.$request->input('tel');
            $detail = 'User contact from first page for test drive';
            $email->sendMailToAdmin($request->input('name'), $detail, now(), 'first page Test Drive ( '.$series->name.') \'s email from '.$request->input('name'), $contact);
            return response()->json('success', 200);
        } catch (\Exception $e) {
            $data['Error'] = $e->getMessage();
            return response()->json($data, 400);
        }
    }
}
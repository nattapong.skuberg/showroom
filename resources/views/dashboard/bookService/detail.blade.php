@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item active">Test Drive</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Book Service</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">
                    @if($book)
                        <h4 class="text-dark header-title m-t-0">{{ $book->created_at }}</h4>
                        <hr>
                        <form class="form-horizontal" role="form" method="post" action="#" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name</label>
                                <div class="col-10">
                                    <input type="text" name="name" class="form-control" value="{{ $book->first_name.' '.$book->last_name }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">E-mail</label>
                                <div class="col-10">
                                    <input type="email" name="email" class="form-control" value="{{ $book->email }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Phone</label>
                                <div class="col-10">
                                    <input type="text" name="phone" class="form-control" value="{{ $book->phone }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Service</label>
                                <div class="col-10">
                                    <input type="text" name="services" id="services" class="form-control" value="" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Book Time</label>
                                <div class="col-10">
                                    <input type="text" name="services" id="services" class="form-control" value="{{ $book->book_time }}" disabled>
                                </div>
                            </div>
                            <hr>

                        </form>


                    @else
                        <h4 class="text-dark header-title m-t-0">Test Drive not found.</h4>
                    @endif
                </div>
            </div>
        </div>


    </div> <!-- end container -->
@endsection

@section('script')
    <script>
        var mes = "";
        var service = JSON.parse("{{ $book->services }}");
        <?php
                $this_service = json_decode($book->services);
                ?>
            @foreach($services as $service)
                @foreach($this_service as $t)
                    mes+= "{{ $t==$service->id?$service->name.', ':'' }}";
                @endforeach
            @endforeach


        $('#services').val(mes);
    </script>
@endsection

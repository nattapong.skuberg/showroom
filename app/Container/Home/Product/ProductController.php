<?php

namespace App\Container\Home\Product;

use App\Container\Dashboard\Post\Models\Post;
use App\Container\Dashboard\Series\Models\Series;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function allProduct()
    {
        $data['series'] = Series::orderByDesc('id')->get();
        $data['products'] = Post::whereNotNull('series_id')->orderByDesc('id')->get();

        return view('home.product.all', $data);
    }

    public function series($id = 0)
    {
        $data['series'] = Series::where('id', $id)->get();
        $data['products'] = Post::whereNotNull('series_id')->where('series_id', $id)->get();

        return view('home.product.all', $data);
    }

    public function product($id = 0)
    {
        $data['post'] = Post::where('id', $id)->whereNotNull('series_id')->first();

        return view('home.post.index', $data);
    }

    public function brochure()
    {
        return view('home.product.brochure');
    }
}
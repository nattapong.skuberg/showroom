<?php

use Illuminate\Database\Seeder;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'product',
            'promotion',
            'news',
            'page',
            'lead'
        ];

        foreach ($types as $item){
            $type = new \App\Container\Dashboard\Post\Models\Post_type();
            $type->name = $item;
            $type->save();
        }
    }
}

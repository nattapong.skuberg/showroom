<?php

namespace App\Container\Dashboard\Post\Controllers;

use App\Http\Controllers\Controller;
use App\Services\FileService;
use Illuminate\Http\Request;

class UploadImageController extends Controller
{
    protected $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function uploadImage(Request $request)
    {
        $file = $request->file('image');

        $url = $this->fileService->storeFile($file, 'img');

        if ($url == null){
            $data['status'] = 'Upload fails.';
        }

        $data['status'] = 'Upload success.';
        $data['url'] = asset($url);

        return response()->json($data, 200);
    }

}
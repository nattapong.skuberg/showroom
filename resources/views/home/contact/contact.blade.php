@extends('layouts.app')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')

    <div class="container mt-4">
        <h2>Contact US</h2>
        <p>ติดต่อเรา</p>
        <div class="row">

            <div class="col-md-8">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d247912.53103332897!2d100.52523522081283!3d13.86103684545125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc6fd18d97af72a61!2sBMW+Performance+Motors!5e0!3m2!1sth!2sth!4v1534752291364" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>    </div>

            <div class="col-md-4">
                <h2>BMW Performance Motors Charansanitwong</h2>
                <address>
                    <p>เพอร์ฟอร์แมนซ์ มอเตอร์ส (จรัญสนิทวงศ์)<br>
                        292 ถ.จรัญสนิทวงศ์ แขวงบางอ้อ เขตบางพลัด กรุงเทพฯ 10700<br>
                        <strong>โทร</strong>: 02-880-0999<br></p>

                </address>
            </div>
        </div>
    </div>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-8">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61986.20774285143!2d100.52778604101945!3d13.830751809891073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29d54358ee5b7%3A0x50e99d3551e08701!2zQk1XIFBlcmZvcm1hbmNlIE1vdG9ycyAo4Liq4Liy4LiC4LiyIOC4o-C4suC4oeC4reC4tOC4meC4l-C4o-C4sik!5e0!3m2!1sth!2sth!4v1534752750598" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div> <div class="col-md-4">
                <h2>BMW Performance Motors Ramintra</h2>
                <address>
                    <p>เพอร์ฟอร์แมนซ์ มอเตอร์ส (รามอินทรา)<br>
                        25/14-15 หมู่ที่ 4 ถ.รามอินทรา ซ.รามอินทรา 21<br>
                        แขวงอนุสาวรีย์ เขตบางเขน  กรุงเทพมหานคร 10220<br>
                        <strong>โทร</strong>: 0-2900-5700 (12 lines)<br>
                        <strong>โทรสาร<strong>: 0-2900-4395 <br></p>

                </address>
            </div>
        </div>
    </div>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-8">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242.09918979166105!2d100.57082516302258!3d13.863809596243014!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29d049513aeef%3A0x1c01366b37d7999d!2sBMW%20Performance%20Motors!5e0!3m2!1sth!2sth!4v1617768606596!5m2!1sth!2sth" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

            </div>
             <div class="col-md-4">
                <h2>BMW Performance Motors Don Mueang</h2>
                <address>
                <p>เพอร์ฟอร์แมนซ์ มอเตอร์ส (ดอนเมือง)<br>
                    9/99 ถนนวิภาวดีรังสิต แขวงตลาดบางเขน <br>
                    เขตหลักสี่ จังหวัดกรุงเทพมหานคร 10210<br>
                    <strong>โทร:</strong> 0-2561-0999 <br></p>

                </address>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection
@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item active">Test Drive</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Test Drive</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">
                        <ul class="nav nav-tabs navtab-bg nav-justified">
                            <li class="nav-item">
                                <a href="#home1" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                    New
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#profile1" data-toggle="tab" aria-expanded="true" class="nav-link">
                                    Approved
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="home1">
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>detail</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($testDrives as $test)
                                        <tr>
                                            <td>
                                                {{ $test->first_name." ".$test->last_name }}
                                            </td>
                                            <td>
                                                {{ $test->created_at }}
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.testDrive.detail',['id'=>$test->id]) }}" class="btn btn-info waves-effect waves-light">detail</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="profile1">
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>detail</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($approved as $test)
                                        <tr>
                                            <td>
                                                {{ $test->first_name." ".$test->last_name }}
                                            </td>
                                            <td>
                                                {{ $test->created_at }}
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.testDrive.detail',['id'=>$test->id]) }}" class="btn btn-info waves-effect waves-light">detail</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @if(count($approved)==0)
                                        <tr>
                                            <td colspan="3">
                                                No Data
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>


    </div> <!-- end container -->
@endsection

@section('script')

@endsection

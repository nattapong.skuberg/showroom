@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item active">Test Drive</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Test Drive</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">
                    @if($test)
                        <h4 class="text-dark header-title m-t-0">{{ $series?$series->name:'Series not found'.' | date : '.$test->created_at }}</h4>
                        <hr>
                        <form class="form-horizontal" role="form" method="post" action="#" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name</label>
                                <div class="col-10">
                                    <input type="text" name="name" class="form-control" value="{{ $test->first_name.' '.$test->last_name }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">E-mail</label>
                                <div class="col-10">
                                    <input type="email" name="email" class="form-control" value="{{ $test->email }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Phone</label>
                                <div class="col-10">
                                    <input type="text" name="phone" class="form-control" value="{{ $test->phone }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Branch</label>
                                <div class="col-10">
                                    <textarea type="text" name="address" class="form-control" disabled>{{ $test->address }}</textarea>
                                </div>
                            </div>
                            {{--<div class="form-group row">--}}
                                {{--<label class="col-2 col-form-label">Province</label>--}}
                                {{--<div class="col-10">--}}
                                    {{--<input type="text" name="Province" class="form-control" disabled value="{{ $province->name }}">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group row">--}}
                                {{--<label class="col-2 col-form-label">District</label>--}}
                                {{--<div class="col-10">--}}
                                    {{--<input type="text" name="District" class="form-control" disabled value="{{ $district->name }}">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group row">--}}
                                {{--<label class="col-2 col-form-label">Sub District</label>--}}
                                {{--<div class="col-10">--}}
                                    {{--<input type="text" name="subdistrict" class="form-control" disabled value="{{ $subdistrict->name }}">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            @if($test->status == 0)
                            <div class="form-group row">
                                <label class="col-2 col-form-label"></label>
                                <div class="col-10">
                                    <a href="{{ route('dashboard.testDrive.approve',['id'=>$test->id,'type'=>1]) }}" class="btn btn-success waves-effect waves-light">Approve</a>
                                    <a href="{{ route('dashboard.testDrive.approve',['id'=>$test->id,'type'=>-1]) }}" class="btn btn-danger waves-effect waves-light">Reject</a>
                                </div>
                            </div>
                            @else
                                @if($test->status == 1)
                                    <button class="btn btn-success waves-effect waves-light" style="width:100%;cursor: auto; border-radius: 0px" disabled>Approved [ {{ $test->updated_at }} ]</button>
                                @else
                                    <button class="btn btn-success waves-effect waves-light" style="width:100%;cursor: auto; border-radius: 0px" disabled>Rejected [ {{ $test->updated_at }} ]</button>
                                @endif
                            @endif
                            <hr>

                        </form>


                    @else
                        <h4 class="text-dark header-title m-t-0">Test Drive not found.</h4>
                    @endif
                </div>
            </div>
        </div>


    </div> <!-- end container -->
@endsection

@section('script')

@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('contact', 'HomeController@contact_page')->name('contact');
Route::get('pricelist', 'HomeController@price_list')->name('pricelist');
Route::group(['prefix' => 'promotion', 'as' => 'promotion.'], function (){
    Route::get('', 'HomeController@promotion')->name('list');
    Route::get('/{id}', ['uses' => 'HomeController@promotionDetail'])->name('detail');
});
Route::group(['prefix' => 'dashboard'], function (){

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('service/book', 'Home\BookService\Controllers\BookServiceController@index')->name('service.book');
    Route::post('service/book', 'Home\BookService\Controllers\BookServiceController@store')->name('service.book');

    Route::get('testdrives', 'Home\TestDrive\Controllers\TestDriveController@index')->name('testdrive');
    Route::post('testdrives', 'Home\TestDrive\Controllers\TestDriveController@store')->name('testdrive');

    Route::get('status/{message}', 'HomeController@statusPage')->name('status');

    Route::group(['prefix' => 'product', 'namespace' => 'Home\Product', 'as' => 'product.'], function (){
        Route::get('/all', ['uses' => 'ProductController@allProduct', 'as' => 'all']);
        Route::get('/series/{id}', ['uses' => 'ProductController@series', 'as' => 'series']);
        Route::get('/product/{id}', ['uses' => 'ProductController@product', 'as' => 'product']);
        Route::get('/brochure', ['uses' => 'ProductController@brochure', 'as' => 'brochure']);
    });

    Route::group(['prefix' => 'post', 'namespace' => 'Home\Post', 'as' => 'post.'], function (){
        Route::get('/all', ['uses' => 'PostController@index', 'as' => 'all']);
        Route::get('/{id}', ['uses' => 'PostController@show', 'as' => 'show']);
    });

    Route::group(['prefix' => 'specialDeal', 'namespace' => 'Home\Post', 'as' => 'specialDeal.'], function (){
        Route::get('/all', ['uses' => 'SpecialDealController@index', 'as' => 'all']);
        Route::get('/{id}', ['uses' => 'SpecialDealController@show', 'as' => 'show']);
    });

    Route::group(['middleware' => 'auth', 'namespace' => 'Dashboard', 'as' => 'dashboard.'], function(){
        Route::get('/', ['uses' => 'Controllers\DashboardController@index', 'as' => 'index']);

        Route::group(['namespace' => 'BookService\Controllers', 'prefix' => 'bookService', 'as' => 'service.'], function (){
            Route::get('/', ['uses' => 'BookServiceController@index', 'as' => 'index']);
            Route::get('/{id}', ['uses' => 'BookServiceController@spec', 'as' => 'index.spec']);
            Route::get('/{id}/detail', ['uses' => 'BookServiceController@detail', 'as' => 'detail']);
        });

        Route::group(['namespace' => 'CarModel\Controllers', 'prefix' => 'model', 'as' => 'model.'], function (){
            Route::get('/', ['uses' => 'CarModelController@index', 'as' => 'index']);
            Route::get('/create', ['uses' => 'CarModelController@create', 'as' => 'create']);
            Route::post('/store', ['uses' => 'CarModelController@store', 'as' => 'store']);
            Route::get('/edit/{id}', ['uses' => 'CarModelController@edit', 'as' => 'edit']);
            Route::post('/update', ['uses' => 'CarModelController@update', 'as' => 'update']);
            Route::get('/delete/{id}', ['uses' => 'CarModelController@destroy', 'as' => 'delete']);
        });

        Route::group(['namespace' => 'News\Controllers', 'prefix' => 'news', 'as' => 'news.'], function (){
            Route::get('/', ['uses' => 'NewsController@index', 'as' => 'index']);
            Route::get('/create', ['uses' => 'NewsController@create', 'as' => 'create']);
            Route::post('/store', ['uses' => 'NewsController@store', 'as' => 'store']);
            Route::get('/edit/{id}', ['uses' => 'NewsController@edit', 'as' => 'edit']);
            Route::post('/update', ['uses' => 'NewsController@update', 'as' => 'update']);
            Route::get('/delete/{id}', ['uses' => 'NewsController@destroy', 'as' => 'delete']);
        });

        Route::group(['namespace' => 'Promotion\Controllers', 'prefix' => 'promotion', 'as' => 'promotion.'], function (){
            Route::get('/', ['uses' => 'PromotionController@index', 'as' => 'index']);
            Route::get('/create', ['uses' => 'PromotionController@create', 'as' => 'create']);
            Route::post('/store', ['uses' => 'PromotionController@store', 'as' => 'store']);
            Route::get('/edit/{id}', ['uses' => 'PromotionController@edit', 'as' => 'edit']);
            Route::post('/update', ['uses' => 'PromotionController@update', 'as' => 'update']);
            Route::get('/delete/{id}', ['uses' => 'PromotionController@destroy', 'as' => 'delete']);
        });

        Route::group(['namespace' => 'Lead\Controllers', 'prefix' => 'lead', 'as' => 'lead.'], function (){
            Route::get('/', ['uses' => 'LeadController@index', 'as' => 'index']);
            Route::get('/create', ['uses' => 'LeadController@create', 'as' => 'create']);
            Route::post('/store', ['uses' => 'LeadController@store', 'as' => 'store']);
            Route::get('/edit/{id}', ['uses' => 'LeadController@edit', 'as' => 'edit']);
            Route::post('/update', ['uses' => 'LeadController@update', 'as' => 'update']);
            Route::get('/delete/{id}', ['uses' => 'LeadController@destroy', 'as' => 'delete']);
            Route::get('/submitted/{id}', ['uses' => 'LeadController@submit', 'as' => 'form']);
        });

        Route::group(['namespace' => 'Page\Controllers', 'prefix' => 'page', 'as' => 'page.'], function (){
            Route::get('/', ['uses' => 'PageController@index', 'as' => 'index']);
            Route::get('/create', ['uses' => 'PageController@create', 'as' => 'create']);
            Route::post('/store', ['uses' => 'PageController@store', 'as' => 'store']);
            Route::get('/edit/{id}', ['uses' => 'PageController@edit', 'as' => 'edit']);
            Route::post('/update', ['uses' => 'PageController@update', 'as' => 'update']);
            Route::get('/delete/{id}', ['uses' => 'PageController@destroy', 'as' => 'delete']);
        });

        Route::group(['namespace' => 'Series\Controllers', 'prefix' => 'series', 'as' => 'series.'], function (){
            Route::get('/', ['uses' => 'SeriesController@index', 'as' => 'index']);
            Route::post('/', ['uses' => 'SeriesController@store', 'as' => 'store']);
            Route::get('/{id}', ['uses' => 'SeriesController@edit', 'as' => 'edit']);
            Route::post('/update', ['uses' => 'SeriesController@update', 'as' => 'update']);
            Route::get('/{id}/delete', ['uses' => 'SeriesController@destroy', 'as' => 'delete']);
        });

        Route::group(['namespace' => 'TestDrive\Controllers', 'prefix' => 'testDrive', 'as' => 'testDrive.'], function (){
            Route::get('/', ['uses' => 'TestDriveController@index', 'as' => 'index']);
            Route::get('/{id}', ['uses' => 'TestDriveController@detail', 'as' => 'detail']);
            Route::get('/approve/{id}/{type}', ['uses' => 'TestDriveController@approve', 'as' => 'approve']);
        });
    });
});

Route::get('/home', function (){
    return redirect()->route('home');
});

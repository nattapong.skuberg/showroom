<?php

namespace App\Services;

use App\Container\Dashboard\Post\Models\Post;
use Illuminate\Support\Facades\Validator;

class PostService
{
    protected $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function validatorCreate($data = [], $type = 1)
    {
        if ($type == 1){
            return Validator::make($data, [
                'series' => 'required',
                'title' => 'required|string',
                'detail' => 'required',
                'cover' => 'required|file|mimes:jpeg,png,jpg,'
            ]);
        }

        return Validator::make($data, [
            'title' => 'required|string',
            'detail' => 'required',
            'cover' => 'required|file|mimes:jpeg,png,jpg,'
        ]);

    }

    public function validatorUpdate($data = [], $type = 1)
    {
        if ($type == 1){
            return Validator::make($data, [
                'series' => 'required',
                'title' => 'required|string',
                'detail' => 'required',
                'cover' => 'file|mimes:jpeg,png,jpg,'
            ]);
        }

        return Validator::make($data, [
            'title' => 'required|string',
            'detail' => 'required',
            'cover' => 'file|mimes:jpeg,png,jpg,'
        ]);
    }

    public function savePost($data = [], $type = 0)
    {
        if (isset($data['id'])){
            $post = Post::find($data['id']);
        }
        else{
            $post = new Post();
        }

        if (isset($data['series'])){
            $post->series_id = $data['series'];
        }
        $post->type_id = $type;
        $post->title = $data['title'];
        $post->detail = $data['detail'];

        if (isset($data['img_url'])){
            $post->image_url_list = $data['img_url'];
        }
        if (isset($data['add_fiels'])){
            $post->additional_field = $data['add_fiels'];
        }
        if (isset($data['cover'])){
            $post->cover_image = $this->fileService->storeFile($data['cover'], 'post_img');
        }
        if (isset($data['og_type'])){
            $post->og_type = $data['og_type'];
        }
        if (isset($data['og_description'])){
            $post->og_description = $data['og_description'];
        }
        if (isset($data['og_image'])){
            $post->og_image = $data['og_image'];
        }

        $post->save();

        return $post;
    }
}
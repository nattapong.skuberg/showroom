<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class FileService
{
    public function storeFile ($file, $directory) {
        $path = $file->hashName($directory);
        Storage::disk('public')->put($directory, $file);
        return Storage::url($path);
    }

}
<?php

namespace App\Container\Dashboard\News\Controllers;

use App\Container\Dashboard\Post\Models\Post;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        $data['posts'] = Post::where('type_id', 3)->get();

        return view('dashboard.news.index', $data);
    }

    public function create()
    {
        $data['type'] = 'News';
        $data['mode'] = 1;
        $data['action'] = route('dashboard.news.store');
        $data['cancel'] = route('dashboard.news.index');


        return view('dashboard.post.index', $data);
    }

    public function edit($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 3)->first();

        if ($post == null){
            return redirect()->back()->withInput('News not found.');
        }

        $data['post'] = $post;
        $data['type'] = 'News';
        $data['mode'] = 2;
        $data['action'] = route('dashboard.news.update');
        $data['cancel'] = route('dashboard.news.index');

        return view('dashboard.post.index', $data);
    }

    public function destroy($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 3)->first();

        if ($post == null){
            return redirect()->back();
        }

        $post->delete();

        return redirect()->route('dashboard.news.index');
    }

    public function store(Request $request)
    {
        $validator = $this->postService->validatorCreate($request->all(), 3);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 3);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'News create fail.']);
        }

        return redirect()->route('dashboard.news.index')->withInput(['msg' => 'Create news success.']);
    }

    public function update(Request $request)
    {
        $validator = $this->postService->validatorUpdate($request->all(), 3);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 3);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'News update fail.']);
        }

        return redirect()->route('dashboard.news.index')->withInput(['msg' => 'Update news success.']);
    }

}
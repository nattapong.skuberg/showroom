<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8" />
    <title>BMW performance - Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    @yield('meta')

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css') }}">
    <link href="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

    @yield('harder')

    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
</head>
<body>

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">

                <!-- Logo container-->
                <div class="logo">
                    <!-- Text Logo -->
                    <!--<a href="index.html" class="logo">-->
                    <!--UBold-->
                    <!--</a>-->
                    <!-- Image Logo -->
                    <a href="{{ route('dashboard.index') }}" class="logo">
                        <img src="{{ asset('assets/images/logo_dark.png') }}" alt="" height="20" class="logo-lg">
                        <img src="{{ asset('assets/images/logo_sm.png') }}" alt="" height="24" class="logo-sm">
                        BMW performance
                    </a>

                </div>
                <!-- End Logo container-->
                @if(\Illuminate\Support\Facades\Auth::user())
                <div class="menu-extras topbar-custom">

                    <ul class="list-inline float-right mb-0">

                        <li class="menu-item list-inline-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <a href="{{ route('logout') }}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-power"></i> <span>Logout</span>

                                </a>

                            </div>
                        </li>

                    </ul>
                </div>
                <!-- end menu-extras -->
                @endif
                <div class="clearfix"></div>

            </div> <!-- end container -->
        </div>
        <!-- end topbar-main -->
        @if(\Illuminate\Support\Facades\Auth::user())
        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">

                        <li class="has-submenu">
                            <a href="{{ route('dashboard.service.index') }}"><i class="fa fa-stethoscope"></i>Book Service</a>
                        </li>

                        <li class="has-submenu">
                            <a href="{{ route('dashboard.testDrive.index') }}"><i class="md md-directions-car"></i>Test Drive</a>
                        </li>

                        <li class="has-submenu">
                            <a href="{{ route('dashboard.series.index') }}"><i class="md md-directions-car"></i>Product</a>
                            <ul class="submenu">
                                <li><a href="{{ route('dashboard.series.index') }}"> Series</a></li>
                                <li><a href="{{ route('dashboard.model.index') }}"> Model</a></li>
                            </ul>
                        </li>

                        <li class="has-submenu">
                            <a href="{{ route('dashboard.page.index') }}"><i class="ti ti-announcement"></i>Page</a>
                        </li>

                        <li class="has-submenu">
                            <a href="{{ route('dashboard.promotion.index') }}"><i class="ti ti-announcement"></i>Promotion</a>
                        </li>

                        <li class="has-submenu">
                            <a href="{{ route('dashboard.news.index') }}"><i class="fa fa-newspaper-o"></i>News</a>
                        </li>
                        <li class="has-submenu">
                            <a href="{{ route('dashboard.lead.index') }}"><i class="fa fa-newspaper-o"></i>Lead</a>
                        </li>

                    </ul>
                    <!-- End navigation menu -->
                </div> <!-- end #navigation -->
            </div> <!-- end container -->
        </div> <!-- end navbar-custom -->
        @endif
    </header>
    <!-- End Navigation Bar-->


    <div class="wrapper">
        @yield('content')
    </div>
    <!-- end wrapper -->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    © 2018 BMW Performance
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- jQuery  -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/tether.min.js') }}"></script><!-- Tether for Bootstrap -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/waves.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>

    <!-- jQuery  -->
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>


    <script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/raphael/raphael-min.js') }}"></script>

    <script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

    @yield('script')

</body>
</html>

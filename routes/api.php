<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/district', ['uses' => 'Home\Address\Controllers\AddressController@getDistrict', 'as' => 'district.all']);
Route::post('/sub-district', ['uses' => 'Home\Address\Controllers\AddressController@getSubDistrict', 'as' => 'subdistrict.all']);
Route::post('/testdrive', ['uses' => 'Home\TestDrive\Controllers\TestDriveController@contactTestDrive', 'as' => 'contact.test']);
Route::group(['namespace' => 'Dashboard\Post\Controllers', 'as' => 'dashboard.post.'], function(){
    Route::post('/upload', ['uses' => 'UploadImageController@uploadImage', 'as' => 'upload']);
});
Route::post('data', 'HomeController@getDataPage')->name('get.data');

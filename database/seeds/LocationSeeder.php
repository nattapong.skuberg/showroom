<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    public function run()
    {
        $reader = \League\Csv\Reader::createFromPath(dirname(__FILE__).'/Location/provinces.csv');
        $reader->setDelimiter(',');

        $results = $reader->getRecords();
        foreach ($results as $result) {
            $province = new \App\Container\Dashboard\TestDrive\Models\Province();
            $province->name = $result[1];
            $province->save();
            break;
        }

        $reader = \League\Csv\Reader::createFromPath(dirname(__FILE__).'/Location/districts.csv');
        $reader->setDelimiter(',');

        $results = $reader->getRecords();
        foreach ($results as $result) {
            $district = new \App\Container\Dashboard\TestDrive\Models\District();
            $district->province_id = $result[3];
            $district->name = $result[1];
            $district->save();
            break;
        }

        $reader = \League\Csv\Reader::createFromPath(dirname(__FILE__).'/Location/sub_districts.csv');
        $reader->setDelimiter(',');

        $results = $reader->getRecords();
        foreach ($results as $result) {
            $subDistrict = new \App\Container\Dashboard\TestDrive\Models\SubDistrict();
            $subDistrict->district_id = $result[3];
            $subDistrict->name = $result[1];
            $subDistrict->save();
            break;
        }

    }
}

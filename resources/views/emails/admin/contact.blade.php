@extends('emails.admin.layouts')
@section('content')
    <p>Contact from : {{ $name }} </p>
    <p> {!! $contact_data !!}</p>
    <p> {!! $detail !!} </p>
    <p>Date : {!! $date !!} </p>
    <br>

    <p> {!! $add !!} </p>

@endsection
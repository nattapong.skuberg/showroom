<?php

namespace App\Container\Dashboard\TestDrive\Controllers;

use App\Container\Dashboard\Series\Models\Series;
use App\Container\Dashboard\TestDrive\Models\District;
use App\Container\Dashboard\TestDrive\Models\Province;
use App\Container\Dashboard\TestDrive\Models\TestDrive;
use App\Http\Controllers\Controller;

class TestDriveController extends Controller
{
    public function index()
    {
        $data['testDrives'] = TestDrive::where('status',0)->get();
        $data['approved'] = TestDrive::where('status',1)->get();
        return view('dashboard.testDrive.index',$data);
    }
    public function detail($id) {
        $data['test'] = TestDrive::find($id);
        if ($data['test'] != null) {
            $sid = $data['test']->series_id;
            $data['series'] = Series::find($sid);
            $data['province'] = Province::find($data['test']->province_id);
            $data['district'] = District::find($data['test']->district_id);
            $data['subdistrict'] = Province::find($data['test']->sub_district_id);
        } else {
            $data['series'] = null;
        }
        return view('dashboard.testDrive.detail', $data);
    }
    public function approve($id,$type) {
        $test = TestDrive::find($id);
        if($test) {
            $test->status = $type == 1 ? 1 : -1;
            $test->save();
            return redirect(route('dashboard.testDrive.index'));
        }
        return redirect()->back()->withErrors('Can\'t update status');
    }
}
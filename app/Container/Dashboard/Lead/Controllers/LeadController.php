<?php

namespace App\Container\Dashboard\Lead\Controllers;

use App\Container\Dashboard\Post\Models\Post;
use App\Http\Controllers\Controller;
use App\Promotion;
use App\Services\PostService;
use Illuminate\Http\Request;

class LeadController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }
    public function submit($id) {
        $data['p'] = Post::find($id);
        $data['posts'] = Promotion::where('posts_id', $id)->orderByDesc('id')->get();
        return view('dashboard.lead.submit-lead', $data);
    }
    public function index()
    {
        $data['posts'] = Post::where('type_id', 5)->get();

        return view('dashboard.lead.index', $data);
    }

    public function create()
    {
        $data['type'] = 'Lead';
        $data['mode'] = 1;
        $data['action'] = route('dashboard.lead.store');
        $data['cancel'] = route('dashboard.lead.index');


        return view('dashboard.post.index', $data);
    }

    public function edit($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 5)->first();

        if ($post == null){
            return redirect()->back()->withInput('lead not found.');
        }

        $data['post'] = $post;
        $data['type'] = 'Lead';
        $data['mode'] = 2;
        $data['action'] = route('dashboard.lead.update');
        $data['cancel'] = route('dashboard.lead.index');

        return view('dashboard.post.index', $data);
    }

    public function destroy($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 5)->first();

        if ($post == null){
            return redirect()->back();
        }

        $post->delete();

        return redirect()->route('dashboard.lead.index');
    }

    public function store(Request $request)
    {
        $validator = $this->postService->validatorCreate($request->all(), 5);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 5);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'lead create fail.']);
        }

        return redirect()->route('dashboard.lead.index')->withInput(['msg' => 'Create lead success.']);
    }

    public function update(Request $request)
    {
        $validator = $this->postService->validatorUpdate($request->all(), 5);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 5);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'lead update fail.']);
        }

        return redirect()->route('dashboard.lead.index')->withInput(['msg' => 'Update lead success.']);
    }

}
@extends('layouts.app')
@section('meta')

@endsection

@section('harder')

@endsection
@section('content')
    <section class="content">
        <div class="container">
            @foreach($series as $s)
                <h3>{{ $s->name }}</h3>
                <div class="row">
                    @foreach($products as $item)
                        @if($item->series_id == $s->id)
                            <div class="col-xs-6 col-md-4 col-lg-3">
                                <a href="{{ route('product.product', ['id' => $item->id]) }}">
                                    <div class="card">
                                        <img class="card-img-top img-fluid" src="{{ asset($item->cover_image) }}">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $item->title }}</h5>
                                        </div>
                                    </div>
                                </a>

                            </div>
                        @endif
                    @endforeach
                </div>
                <hr class="hr-end">
            @endforeach
        </div>
    </section>
@endsection
@section('script')

@endsection

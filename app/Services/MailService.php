<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;

class MailService
{
    public function sendMail ($text, $first_name, $last_name, $email, $subject, $title = 'bmw performance motors thailand')
    {

        $data = [
            'text' => $text,
            'subject'=> $subject,
            'title'=> $title,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email
        ];
        Mail::send('emails.template', $data, function ($message) use ($data) {
            $message->from('noreply@bmwperformance.com', $data['title']);
            $message->to($data['email'])->subject($data['subject']);
        });
    }
    public function sendMailToAdmin ($name, $detail, $date = null, $subject = 'Email from customer', $contact_data = '', $add = '', $title = 'bmw performance motors thailand')
    {
        $date = $date ? now() : '';
//        $email = [
//            'pishanan.poreekaew@bmw-performancemotors.com',
//            'caramell607@gmail.com',
//            'patcharee.sangthong@bmw-performancemotors.com',
//            'sutasini.wo@simedarby.com'
//        ];
        $email = [
            'niramon.wichitdechakun@bmw-performancemotors.com',
            'caramell607@gmail.com'
        ];
//        $email2 = ['phittayajenkhetkarn@gmail.com'];
        foreach ($email as $e) {
//            dd($e);
            $data = [
                'date' => $date,
                'name' => $name,
                'detail' => $detail,
                'contact_data' => $contact_data,
                'add' => $add,
                'subject' => $subject,
                'title' => $title,
                'email' => $e
            ];
            Mail::send('emails.admin.contact', $data, function ($message) use ($data) {
                $message->from('noreply@bmwperformance.com', $data['title']);
                $message->to($data['email'])->subject($data['subject']);
            });
        }
    }

}
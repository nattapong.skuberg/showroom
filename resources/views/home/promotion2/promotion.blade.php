@extends('layouts.app')
@section('content')
    <section class="ads-one">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>--}}
                {{--<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>--}}
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{ $post->cover_image }}" alt="First slide">
                </div>
            </div>
            <a class="carousel-control-prev d-none" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next d-none" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <section class="form py-5">
        <div class="container">
            <h2>{{ $post->title }}</h2>
            <br>
            {!! $post->detail !!}
            <hr>
            <h3>ลงทะเบียนเพื่อรับข้อเสนอพิเศษ</h3>
            <form class="form-register" id="sub-form">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control" placeholder="ชื่อ - นามสกุล*" name="name" id="name">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control" placeholder="เบอร์โทรศัพท์ *" name="phone" id="phone">
                    </div>
                </div>
                <input name="post_id" value="{{ $post->id }}" hidden>
                <div class="form-row">
                    <?php
                    $adds = json_decode($post->additional_field);
                    ?>
                    <div class="form-row col-md-12">
                        <input type="hidden" value="{{ $post->additional_field }}" id="add_field">
                        @if($adds)
                            @foreach($adds as $add)
                                <div class="form-group col-md-6">
                                    <label>{{ $add }}</label>
                                    <input type="text" class="form-control" placeholder="{{ $add }}" name="{{ $add }}"
                                           id="{{ $add }}">
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label>รุ่นที่สนใจ *</label>
                        <input type="email" class="form-control" placeholder="รุ่นที่สนใจ *" name="interest"
                               id="interest">
                    </div>
                    <div class="form-group col-md-6">
                        <label>สนใจทดลองขับ *</label>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios"
                                           value="1" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                        สนใจ
                                    </label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios"
                                           value="2" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                        ไม่สนใจ
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-bmw" onclick="sendData()" id="btn-submit">Submit
                        <span id="loading"></span></button>
                </div>
            </form>
        </div>
    </section>
    {{--<section class="blog py-5">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col col-md-4">--}}
    {{--<div class="content-blog">--}}
    {{--<a href="#">--}}
    {{--<div class="content-overlay"></div>--}}
    {{--<img src="{{ asset('images/Dealers-01.jpg') }}" class="d-block mx-auto" width="350">--}}
    {{--<div class="content-details fadeIn-bottom">--}}
    {{--<h3 class="content-title">This is a title</h3>--}}
    {{--<p class="content-text">This is a short description</p>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col col-md-4">--}}
    {{--<div class="content-blog">--}}
    {{--<a href="#">--}}
    {{--<div class="content-overlay"></div>--}}
    {{--<img src="{{ asset('images/Dealers-03.jpg') }}" class="d-block mx-auto" width="350">--}}
    {{--<div class="content-details fadeIn-bottom">--}}
    {{--<h3 class="content-title">This is a title</h3>--}}
    {{--<p class="content-text">This is a short description</p>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col col-md-4">--}}
    {{--<div class="content-blog">--}}
    {{--<a href="#">--}}
    {{--<div class="content-overlay"></div>--}}
    {{--<img src="{{ asset('images/Dealers-04.jpg') }}" class="d-block mx-auto" width="350">--}}
    {{--<div class="content-details fadeIn-bottom">--}}
    {{--<h3 class="content-title">This is a title</h3>--}}
    {{--<p class="content-text">This is a short description</p>--}}
    {{--</div>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
@endsection
@section('custom-script')
    <script>
        function sendData() {
            const name = $('#name').val();
            const phone = $('#phone').val();
            const interest = $('#interest').val();
            var add_data = [];
            const send_add = {};
            try {
                add_data = JSON.parse($('#add_field').val());
                for (var i = 0; i < add_data.length; i++) {
                    send_add[add_data[i]] = $('#' + add_data[i]).val();
                }
            } catch (err) {
                add_data = [];
            }
            if (name.length == 0 || phone.length == 0 || interest.length == 0) {
                Swal.fire({
                    type: 'error',
                    title: '',
                    text: 'กรุณากรอกข้อมูลให้ครบ'
                });
            }
            else if (phone.length < 10 || phone.length > 12) {
                Swal.fire({
                    type: 'error',
                    title: '',
                    text: 'กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง'
                });
            } else {
                $('#btn-submit').prop('disable', 'true');
                $('#loading').html('<i class="fa fa-spinner fa-spin"></i>');
                const data = {
                    name: $('#name').val(),
                    phone: $('#phone').val(),
                    type: $('#interest').val(),
                    interest: $('input[name="gridRadios"]:checked').val(),
                    post_id: '{{ $post->id }}',
                    add_data: JSON.stringify(send_add)
                };
                $.ajax({
                    type: "POST",
                    url: "{{ route('get.data') }}",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        Swal.fire({
                            type: 'success',
                            title: 'ขอบคุณ',
                            text: 'ลงทะเบียนเรียบร้อยแล้ว',
                        });
                        $('#btn-submit').prop('disable', 'false');
                        $('#loading').html('');
                        $('#name').val('');
                        $('#phone').val('');
                        $('#interest').val('');
                    },
                    error: function (errMsg) {
//                    console.log(errMsg);
                        Swal.fire({
                            type: 'error',
                            title: 'ข้อผิดพลาด',
                            text: 'ลงทะเบียนไม่สำเร็จ',
                        });
                        $('#btn-submit').prop('disable', 'false');
                        $('#loading').html('');
                    }
                });
            }

        }

    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KX8QBPL"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
@endsection
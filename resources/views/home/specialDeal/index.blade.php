@extends('layouts.app')

@section('meta')
    <meta property="og:title" content="{{ $post->title }}">
    <meta property="og:description" content="{{ $post->og_description }}">
    <meta property="og:image" content="{{ $post->og_image }}">
    <meta property="og:type" content="{{ $post->og_type }}">
    <title></title>
@endsection

@section('harder')

@endsection

@section('content')
    <section class="content">
        <div class="container">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    @if(json_decode($post->image_url_list) != null)
                        @foreach(json_decode($post->image_url_list) as $index => $url)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $index+1 }}"></li>
                        @endforeach
                    @endif
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item slide-bg active " style="background: url('{{ asset($post->cover_image) }}') no-repeat; background-size: cover; background-position: 0px;"></div>
                    @if(json_decode($post->image_url_list) != null)
                        @foreach(json_decode($post->image_url_list) as $index => $url)
                            <div class="carousel-item slide-bg" style="background: url('{{ $url }}') no-repeat; background-size: cover; background-position: 0px;"></div>
                        @endforeach
                    @endif
                </div>
            </div>

            <h3>{{ $post->title }}</h3>
            {!! $post->detail !!}
            <p>update at {{ $post->updated_at }}</p>

        </div>
    </section>
@endsection

@section('script')

@endsection
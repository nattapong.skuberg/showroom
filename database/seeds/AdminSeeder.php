<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'dev';
        $user->email = 'dev@dev.com';
        $user->password = bcrypt('123456');
        $user->save();
    }
}

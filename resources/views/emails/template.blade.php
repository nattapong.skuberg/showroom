<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width-device-width, initail-scale=1">
</head>
<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            {{--header--}}
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td>
                        {{--<img src="{{ asset('images/mail_header.png') }}" width="100%" style="display: block;margin:0;padding:0;">--}}
                    </td>
                </tr>
            </table>

            {{--title--}}
            {{--greet--}}
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="height: 50px;font-size: 16px">เรียน, {{$first_name}} {{$last_name}}<br><br></td>
                </tr>
            </table>
            {{--content--}}
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td> {{ $text }}</td>
                </tr>
            </table>
            <br>
            <br>

            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td style="height: 50px;font-size: 16px">Contact us : info@bmwperformance.com<br><br>	</td>
                </tr>
            </table>
            {{--regrade--}}
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td style="height: 50px;font-size: 16px">Sincerely<br>
                        bmw-performancemotors.com<br>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
            </table>

            {{--footer--}}
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="height: 50px;font-size: 16px;text-align: center;color: #ffffff" bgcolor="#A9A9A9";> Copyright &copy; 2018  www.bmw-performancemotors.com All rights reserved.</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>

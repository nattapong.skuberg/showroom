<?php

namespace App\Container;

use App\Container\Dashboard\Post\Models\Post;
use App\Container\Dashboard\Series\Models\Series;
use App\Http\Controllers\Controller;
use App\Promotion;
use App\Services\MailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $data['series'] = Series::orderByDesc('id')->get();
        $data['news'] = Post::where('type_id', 3)->orderByDesc('created_at')->limit(4)->get();

        return view('home', $data);
    }
    public function statusPage($message) {
        $data['message'] = $message;
        return view('home.status.index',$data);
    }
    public function contact_page() {
        return view('home.contact.contact');
    }
    public function price_list() {
        return view('home.price.priceList');
    }
    public function promotion() {
        $data['posts'] = Post::where('type_id', 5)->orderByDesc('id')->paginate();
        return view('home.promotion.list', $data);

    }
    public function promotionDetail($id) {
        $data['post'] = Post::find($id);
        return view('home.promotion.promotion', $data);
    }
    public function getDataPage(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required | min :10 | max: 12',
            'type' => 'required',
            'interest' => 'required',
            'post_id' => 'required'
        ]);
//        return response()->json('success');
        if ($validator->fails()) {
            return response()
                ->json($validator->getMessageBag(), 400);
        } else {
            $form = new Promotion();
            $form->name = $request->input('name', '');
            $form->phone = $request->input('phone', '');
            $form->type = $request->input('type', '');
            $form->is_interest = $request->input('interest', '');
            $form->posts_id = $request->input('post_id', '');
            if ($request->input('add_data', '')) {
                $form->additional_field = $request->input('add_data', '');
            }
            $form->save();
            $data['form'] = $form;
            $data['mgs'] = 'success';
            try {
                $email = new MailService();
                $data['post'] = Post::find($request->input('post_id', ''));
                if ($data['post']) {
                    $contact = 'Tel : '.$request->input('phone');
                    $interest = ($form->is_interest == 1 )? '' : 'not';
                    $detail = 'User contact from promotion and '.$interest.' interest to test drive<br> More detail link : '.route('dashboard.lead.form', ['id' => $data['post']->id]);
                    $email->sendMailToAdmin($request->input('name'), $detail, now(), $data['post']->title, $contact);
                }

            } catch (\Exception $exception) {
                return response()->json($exception->getMessage(), 404);
            }
            return response()->json($data);
        }
    }
}

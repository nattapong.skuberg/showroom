<?php

namespace App\Container\Dashboard\CarModel\Controllers;

use App\Container\Dashboard\Post\Models\Post;
use App\Container\Dashboard\Series\Models\Series;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use Illuminate\Http\Request;

class CarModelController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        $data['series'] = Series::orderByDesc('id')->get();
        $data['models'] = Post::where('type_id', 1)->get();

        return view('dashboard.product.model', $data);
    }

    public function create()
    {
        $data['type'] = 'Model';
        $data['mode'] = 1;
        $data['action'] = route('dashboard.model.store');
        $data['cancel'] = route('dashboard.model.index');

        $data['series'] = Series::all();

        return view('dashboard.post.index', $data);
    }

    public function edit($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 1)->first();

        if ($post == null){
            return redirect()->back()->withInput('Model not found.');
        }

        $data['post'] = $post;
        $data['type'] = 'Model';
        $data['mode'] = 2;
        $data['action'] = route('dashboard.model.update');
        $data['cancel'] = route('dashboard.model.index');

        $data['series'] = Series::all();

        return view('dashboard.post.index', $data);
    }

    public function destroy($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 1)->first();

        if ($post == null){
            return redirect()->back();
        }

        $post->delete();

        return redirect()->route('dashboard.model.index');
    }

    public function store(Request $request)
    {
        $validator = $this->postService->validatorCreate($request->all());

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 1);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'Model create fail.']);
        }

        return redirect()->route('dashboard.model.index')->withInput(['msg' => 'Create model success.']);
    }

    public function update(Request $request)
    {
        $validator = $this->postService->validatorUpdate($request->all());

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 1);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'Model update fail.']);
        }

        return redirect()->route('dashboard.model.index')->withInput(['msg' => 'Update model success.']);
    }
}
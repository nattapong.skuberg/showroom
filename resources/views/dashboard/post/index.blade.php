@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')
    <style>
        .md {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a>
                            </li>
                            @if($type == 'Model')
                                <li class="breadcrumb-item"><a
                                            href="{{ route('dashboard.model.index') }}">{{ $type }}</a></li>
                            @elseif($type == 'Page')
                                <li class="breadcrumb-item"><a
                                            href="{{ route('dashboard.promotion.index') }}">{{ $type }}</a></li>
                            @elseif($type == 'News')
                                <li class="breadcrumb-item"><a
                                            href="{{ route('dashboard.news.index') }}">{{ $type }}</a></li>
                            @endif
                            <li class="breadcrumb-item active">{{ ($mode == 1)? 'Create' : 'Edit' }}</li>
                        </ol>
                    </div>
                    <h4 class="page-title">{{ $type }}</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Image for {{ $type }}</b></h4>
                    <br>
                    <table class="table table-bordered" id="image_table">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>URL</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="3" style="text-align: center;">No image.</td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-md-8 offset-2">
                            <form class="form-horizontal" id="PostImage">
                                <div class="row">
                                    <label class="control-label">Upload image</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-9">
                                        <input type="file" class="filestyle" name="image"
                                               data-placeholder="Series cover" id="filestyle-9" tabindex="-1"
                                               style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-default">Upload</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        @if(isset($type) && $type == 'Lead')
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Form setting</b></h4>
                        <br>
                        <table class="table table-bordered" id="form_table">
                            <thead>
                            <tr>
                                <th>name</th>
                                <th>type</th>
                                <th>Tools</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="3" style="text-align: center;">No Data.</td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-6 offset-2">
                                {{--<form class="form-horizontal" id="PostImage">--}}
                                    <div class="row">
                                        <label class="control-label">Add new</label>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="form_name" id="form_name">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" class="btn btn-default" onclick="addOption()">Add</button>
                                        </div>
                                    </div>
                                {{--</form>--}}
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>{{ $type }} information</b></h4>
                    <br>

                    <form role="form" name="blog_form" id="blog_form" class="profile-form" method="post"
                          action="{{ $action }}" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
                        {{ csrf_field() }}

                        <input type="hidden" id="img_url" name="img_url"
                               value="{{ (isset($post) && old('img_url') == '')? $post->image_url_list : old('img_url') }}"/>
                        <input type="hidden" name="id" value="{{ (isset($post))? $post->id : '' }}"/>

                        <div class="form-group">
                            <div class="box-picture" style="text-align: center;">
                                <div class="box-thumbnail">
                                <span id="img_tag">
                                @if(isset($post))
                                        <img src="{{ $post->cover_image }}" style="max-width: -webkit-fill-available">
                                    @else
                                        <i class="fa fa-camera fa-5x"></i>
                                    @endif
                                </span>
                                </div>
                                <p>Cover image size</p>
                                <div class="box-btn">
                                    <input type="file" name="cover" id="file-8" class="inputfile inputfile-6"
                                           data-multiple-caption="{count} files selected" style="display: none;"
                                           multiple/>
                                    <label for="file-8"><span></span> <strong>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                                 viewBox="0 0 20 17">
                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                            Choose a file&hellip;</strong></label>
                                </div>
                                @if ($errors->has('cover'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cover') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @if($type == 'Model')
                            <div class="form-group">
                                <label for="title">Series</label>
                                <select name="series" id="series" class="form-control">
                                    @foreach($series as $item)
                                        <option value="{{ $item->id }}" {{ (isset($post) && old('series') == '')? (($post->series_id == $item->id)? 'selected' : '') : ((old('series') == $item->id)? 'selected' : '') }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('series'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('series') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Enter title"
                                   value="{{ (isset($post) && old('title') == '')? $post->title : old('title') }}"/>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="detail">Detail</label>
                            <textarea name="detail"
                                      id="detail">{{ (isset($post) && old('detail') == '')? $post->detail : old('detail') }}</textarea>
                            @if ($errors->has('detail'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('detail') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="seo_og_type">SEO OG Type</label>
                            <input type="text" class="form-control" name="og_type" id="og_type"
                                   placeholder="Enter og type"
                                   value="{{ (isset($post) && old('og_type') == '')? $post->og_type : old('og_type') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="seo_description">SEO OG Description</label>
                            <input type="text" class="form-control" name="og_description" id="og_description"
                                   placeholder="Enter description"
                                   value="{{ (isset($post) && old('og_description') == '')? $post->og_type : old('og_description') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="seo_keyword">SEO OG Image</label>
                            <input type="text" class="form-control" name="og_image" id="og_image"
                                   placeholder="Enter image url"
                                   value="{{ (isset($post) && old('og_image') == '')? $post->og_image : old('og_image') }}"/>
                        </div>
                        <input type="hidden" name="add_fiels" id="add_fiels" value="{{ (isset($post) ? $post->additional_field : '') }}">
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-success"><i class="fa fa-save"></i>&nbsp;Save
                            </button>
                            <a href="{{ $cancel }}" class="btn btn-lg btn-default"><i class="fa fa-close"></i>&nbsp;Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{--modal--}}
        <div class="modal fade" id="upload_warning">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>
                            <i class="fa fa-warning"></i>&nbsp;Upload waining
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h3>
                    </div>
                    <div class="modal-body">
                        <p id="warning_text"></p>
                    </div>
                </div>
            </div>
        </div>
        {{--end model--}}
    </div> <!-- end container -->
@endsection

@section('script')
    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"
            type="text/javascript"></script>

    <script>

        var list_img_name = [];
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img_tag').html('<img src="" id="img_upload" width="50%">');
                    $('#img_upload').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#file-8").change(function () {
            readURL(this);
        });

    </script>
    <script>
        // initSample();
        CKEDITOR.replace('detail');
        CKEDITOR.config.height = 400;
    </script>

    <script>
        options = [];
        initOption();
        function initOption() {
            try {
                var d = $('#add_fiels').val();
                options = JSON.parse(d);
            } catch (err) {
                options = [];
            }
            renderHtml();
        }
        function deleteOption(txt) {
            var tmp = [];
            for (var i = 0 ; i < options.length ; i++) {
                console.log(txt, options[i], options[i] === txt)
                if (options[i] === txt) {
                } else {
                    tmp.push(options[i]);
                }
            }
            options = tmp;
            renderHtml();
        }
        function addOption() {
            var op = $('#form_name').val();
            var find = false;
            for (var i = 0 ; i < options.length ; i++) {
                if (options[i] === op) {
                    find = true;
                    break;
                }
            }
            if (!find) {
                options.push(op);
                renderHtml();
            }
        }
        function renderHtml() {
            $('#add_fiels').val(JSON.stringify(options));
            var htm = '';
            console.log(options);
            for (var i = 0 ; i < options.length ; i++) {
                htm += '<tr><td>' + options[i] + '</td><td>text</td><td><i class="md md-delete" onclick="deleteOption(\'' + options[i] + '\')"></i></td></tr>';
            }
            if (options.length === 0) {
                htm = ' <tr>\n' +
                    '                                <td colspan="3" style="text-align: center;">No Data.</td>\n' +
                    '                            </tr>';
            }
            $('#form_table tbody').html(htm);
        }
        function Delete(key) {
            list_img_name = $.grep(list_img_name, function (e) {
                return e != key;
            });

            var imglist = list_img_name;
            $('#image_table tbody').replaceWith("<tbody></tbody>");
            for (i = 0; i < imglist.length; i++) {
                $('#image_table tbody').append('<tr><td><img src="' + imglist[i] + '" width="60" height="60"></td><td>' + imglist[i] + '</td><td><a onclick="Delete(\'' + imglist[i] + '\')">ลบ</a></td></tr>');
            }
            $('#img_url').val(JSON.stringify(list_img_name));
            $(this).dialog("close");

        }

        function initImageList() {
                <?php
                if($mode == 2) {
                ?>
                var imglist = '{!! $post->image_url_list !!}';
                if (imglist.length === 0) {
                    imglist = '[]';
                }
                imglist = JSON.parse(imglist);
                list_img_name = imglist;

                $('#image_table tbody').replaceWith("<tbody></tbody>");
                for (i = 0; i < imglist.length; i++) {
                    $('#image_table tbody').append('<tr><td><img src="' + imglist[i] + '" width="60" height="60"></td><td>' + imglist[i] + '</td><td><a onclick="Delete(\'' + imglist[i] + '\')">ลบ</a></td></tr>');
                }

            <?php
            }
            ?>
        }
        initImageList();

        $('#PostImage').submit(function (event) {
            $.ajax({
                type: "POST",
                url: "{{ route('dashboard.post.upload') }}",
                // The key needs to match your method's input parameter (case-sensitive).
                data: new FormData($("#PostImage")[0]),
                dataType: 'json',
                async: false,
                processData: false,
                contentType: false,

                success: function (res) {
                    if (typeof(Storage) !== "undefined") {
                        $('#image_table tbody').append('<tr class="child"><td><img src="' + res.url + '" width="60" height="60" ></td><td>' + res.url + '</td><td><a onclick="Delete(\'' + res.url + '\')"> ลบ</a></td></tr>');
                        $('#image').val("");

                        list_img_name.push(res.url);
                        $('#img_url').val(JSON.stringify(list_img_name));


                    } else {
                        // Sorry! No Web Storage support..
                        alert('Sorry! No Web Storage support.');
                    }
//                    console.log(res);

                },
                error: function (err) {
                    var error = err.responseJSON;
                    alert('error : ' + error);
                }
            });
            return false;
        });
    </script>
@endsection

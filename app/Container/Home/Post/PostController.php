<?php

namespace App\Container\Home\Post;

use App\Container\Dashboard\Post\Models\Post;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function index()
    {
        $data['posts'] = Post::where('type_id', 3)->OrderByDesc('created_at')->get();

        return view('home.post.all', $data);
    }

    public function show($id = 0)
    {
        $data['post'] = Post::where('id', $id)->whereNull('series_id')->first();

        return view('home.post.index', $data);
    }
}
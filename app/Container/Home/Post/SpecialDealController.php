<?php

namespace App\Container\Home\Post;

use App\Container\Dashboard\Post\Models\Post;
use App\Http\Controllers\Controller;

class SpecialDealController extends Controller
{
    public function index()
    {
        $data['posts'] = Post::where('type_id', 2)->OrderByDesc('created_at')->get();

        return view('home.specialDeal.all', $data);
    }

    public function show($id = 0)
    {
        $data['post'] = Post::where('id', $id)->where('type_id', 2)->whereNull('series_id')->first();

        return view('home.specialDeal.index', $data);
    }
}
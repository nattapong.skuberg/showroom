@extends('layouts.app')
@section('custom-style')
    <style>
        .time-confirm {
        width: 295px;
        border: 1px solid #ccc;
        margin: 0 auto;
        float: none;
        cursor: pointer;
            text-align: center;
        }
        .step-box {
            display: none;
        }
        .btn {
            border: 1px solid gray;
        }
        .form-step {
            margin-top: 1rem;
            margin-bottom: 1rem;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')

    <div class="container">
        <form method="post" action="{{ route('service.book') }}">
            {{ csrf_field() }}
            <div class="col-md-12 form-step">
                <div class="col-md-12 step-box" id="step1">
                    <h3> เลือกบริการที่ท่านต้องการ </h3>
                    <hr>

                @foreach($services as $service)
                        <div class="checkbox">
                            <label><input type="checkbox" name="service{{ $service->id }}" id="service{{ $service->id }}" value="{{ $service->id }}"> {{ $service->name }}</label>
                        </div>
                    @endforeach
                    <hr>
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn" onclick="next()">ถัดไป</a>
                    </div>
                </div>
                <div class="col-md-12 step-box" id="step2">
                    <h3> เลือกวันที่และเวลา </h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <input id="datepicker" width="276" onchange="dateChange()"/>
                            <script>
                                $('#datepicker').datetimepicker({
                                    uiLibrary: 'bootstrap4'
                                });
                            </script>
                        </div>
                        <div class="col-md-6">
                            <input  id="book-time" hidden>

                        </div>
                        <div class="col-md-12">
                            <p>หมายเหตุ</p>
                            <p>1. หากท่านนำรถเข้าบริการช้ากว่าเวลาที่นัดหมายไว้ เกินกว่า 15 นาที บริษัทฯ ขอสงวนสิทธิ์ยกเลิกนัด โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>
                            <p>2. กรุณาทำการนัดหมายล่วงหน้าอย่างน้อย 2 วัน</p>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn" onclick="back()">ย้อนกลับ</a>
                        <a href="javascript:void(0)" class="btn" onclick="next()">ถัดไป</a>
                    </div>
                </div>
                <div class="col-md-12 step-box" id="step3">
                    <h3>ยืนยัน วันที่และเวลา</h3>
                    <hr>
                    <div class="time-confirm">
                        <div class="col-md-12">
                            <input id="datepicker2" name="book-date" width="276" />
                            <script>
                                $('#datepicker2').datetimepicker({
                                    uiLibrary: 'bootstrap4'
                                });
                            </script>
                        </div>
                    </div>
                    {{--<div class="time-confirm">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<h4>11.00 น.</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-md-12">
                        <p>หมายเหตุ</p>
                        <p>1. หากท่านนำรถเข้าบริการช้ากว่าเวลาที่นัดหมายไว้ เกินกว่า 15 นาที บริษัทฯ ขอสงวนสิทธิ์ยกเลิกนัด โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>
                        <p>2. กรุณาทำการนัดหมายล่วงหน้าอย่างน้อย 2 วัน</p>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn" onclick="back()">ย้อนกลับ</a>
                        <a href="javascript:void(0)" class="btn" onclick="next()">ถัดไป</a>
                    </div>
                </div>
                <div class="col-md-12 step-box" id="step4">
                    <h3>กรุณากรอกข้อมูล</h3>
                    <hr>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="inputAddress">ชื่อ</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">นามสกุล</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="lastname">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">อีเมลล์</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">เบอร์โทรศัพท์มือถือ</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="phone">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">หมายเลขตัวถังหรือ ป้ายทะเบียนรถของคุณ</label>
                            <input type="text" class="form-control" id="model" name="model" placeholder="number">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn" onclick="back()">ย้อนกลับ</a>
                        <button class="btn" type="submit">ส่งคำขอ</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@section('custom-script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

    <script>
        currentBox = 1;
        $( document ).ready(function() {
            $("#step1").css("display", "block");
        });
        function render() {
            if (currentBox > 4) {
                currentBox = 4;
            }
            for (var i = 1; i <= 4; i++) {
                $("#step" + i).css("display", i==currentBox?"block":"none");
                console.log($("#step" + i));
            }
            return false;
        }
        function next() {
            currentBox++;
            render();
        }
        function back() {
            currentBox--;
            render();
        }
        function dateChange() {
//            console.log()
            $('#datepicker2').val($('#datepicker').val());
        }
    </script>
@endsection
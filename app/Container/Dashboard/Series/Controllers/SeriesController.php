<?php

namespace App\Container\Dashboard\Series\Controllers;

use App\Container\Dashboard\Series\Models\Series;
use App\Http\Controllers\Controller;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SeriesController extends Controller
{
    protected $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        $data['series'] = Series::all();

        return view('dashboard.product.series', $data);
    }

    public function edit($id = 0)
    {
        $data['series'] = Series::all();
        $data['temp'] = Series::find($id);

        if ($data['temp'] == null){
            return redirect()->back();
        }

        return view('dashboard.product.series', $data);
    }

    public function store(Request $request)
    {
        $validator = $this->validatorStore($request->all());

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $series = $this->saveSeries($request->all());

        if ($series == null){
            return redirect()->back()->withInput($request->all())->withErrors(['cover' => 'Create series fail.']);
        }

        return redirect()->route('dashboard.series.index');
    }

    public function update(Request $request)
    {
        $validator = $this->validatorUpdate($request->all());

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $series = $this->saveSeries($request->all());

        if ($series == null){
            return redirect()->back()->withInput($request->all())->withErrors(['cover' => 'Create series fail.']);
        }

        return redirect()->route('dashboard.series.index');
    }

    public function destroy($id = 0)
    {
        $data['temp'] = Series::find($id);

        if ($data['temp'] == null){
            return redirect()->back();
        }

        $data['temp']->delete();

        return redirect()->route('dashboard.series.index');
    }

    protected function validatorStore($data = [])
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|unique:series,name',
            'cover' => 'required|file|mimes:jpeg,png,jpg',
            'cover2' => 'required|file|mimes:jpeg,png,jpg'
        ]);
    }

    protected function validatorUpdate($data = [])
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'cover' => 'file|mimes:jpeg,png,jpg,',
            'cover2' => 'file|mimes:jpeg,png,jpg,'
        ]);
    }

    protected function saveSeries($data =[])
    {
        if (isset($data['id'])){
            $series = Series::find($data['id']);
        }
        else{
            $series = new Series();
        }

        $series->name = $data['name'];
        if (isset($data['cover'])){
            $series->cover = $this->fileService->storeFile($data['cover'], 'series_cover');
        }
        if (isset($data['cover2'])){
            $series->cover2 = $this->fileService->storeFile($data['cover2'], 'series_cover2');
        }

        $series->save();

        return $series;
    }

}
@extends('layouts.app')
@section('custom-style')
    <style>
        .message-center {
            text-align: center;
        }
    </style>
@endsection
@section('content')

    <div class="container message-center">
        {{ $message }}
    </div>
@endsection
@section('custom-script')
    <script>
    </script>
@endsection
<?php

namespace App\Container\Dashboard\TestDrive\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $timestamps = false;
}

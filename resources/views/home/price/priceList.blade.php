@extends('layouts.app')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <div class="container">
        <h1>BMW PRICE LIST.</h1>
        <p>Effective from 23 September 2021</p>
        <h4>Retail Price List</h4>
        <p>BMW Hire Purchase(with Balloon)Monthly Payment***/**** (THB)ค่าใช้จ่ายต่อเดือน***/**** (บาท)</p>
        
        <table class="table table-hover">
        
            <thead>
            <tr>
                <th scope="col" style="width:35%">รุ่น</th>
                <th scope="col" style="text-align: right;">Engine (CC)เครื่องยนต์ (ซีซี)</th>
                <th scope="col" style="text-align: right;">SuggestedRetail Price* (THB) ราคาขายปลีกแนะนำา* (บาท)</th>
                <th scope="col" style="text-align: right;">Price with BSISTANDARD package** (THB) ราคาขายรวม BSI STANDARD แพคเกจ (บาท)</th>
                <th scope="col" style="text-align: right;">60 Months/60 เดือน</th>
            </tr>
            </thead>
            <tbody>
            <!-- <tr>
                <th scope="row">118i M Sport (M Performance Edition)</th>
                <td><div class="textright">1,499</div></td>
                <td><div class="textright">2,069,000</div></td>
                <td><div class="textright">2,099,000</div></td>
                <td><div class="textright">18,999</div></td>
            </tr> -->
            <!-- <tr>
                <th scope="row">218i Gran Coupé M Sport</th>
                <td><div class="textright">1,499</div></td>
                <td><div class="textright">2,369,000</div></td>
                <td><div class="textright">2,399,000</div></td>
                <td><div class="textright">20,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">220i Gran Coupé Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">1,969,000</div></td>
                <td><div class="textright">1,999,000</div></td>
                <td><div class="textright">17,999</div></td>
            </tr>
            <tr>
                <th scope="row">220i Gran Coupé M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,139,000</div></td>
                <td><div class="textright">2,169,000</div></td>
                <td><div class="textright">18,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">320d GT Sport</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">2,269,000</div></td>
                <td><div class="textright">2,299,000</div></td>
                <td><div class="textright">19,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">320d M Sport</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">2,519,000</div></td>
                <td><div class="textright">2,549,000</div></td>
                <td><div class="textright">21,999</div></td>
            </tr>
            
            <tr>
                <th scope="row">330e M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,769,000</div></td>
                <td><div class="textright">2,799,000</div></td>
                <td><div class="textright">24,999</div></td>
            </tr>
            <tr>
                <th scope="row">330e M Sport (M Performance Edition)</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,969,000</div></td>
                <td><div class="textright">2,999,000</div></td>
                <td><div class="textright">25,999</div></td>
            </tr>
            <tr>
                <th scope="row">320Li Luxury</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,439,000</div></td>
                <td><div class="textright">2,469,000</div></td>
                <td><div class="textright">21,999</div></td>
            </tr>
            <tr>
                <th scope="row">330Li M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,869,000</div></td>
                <td><div class="textright">2,899,000</div></td>
                <td><div class="textright">24,999</div></td>
            </tr>
            <tr>
                <th scope="row">M340i xDrive</th>
                <td><div class="textright">2,998</div></td>
                <td><div class="textright">3,969,000</div></td>
                <td><div class="textright">3,999,000</div></td>
                <td><div class="textright">34,999</div></td>
            </tr>
           
            <tr>
                <th scope="row">430i Coupé M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">3,939,000</div></td>
                <td><div class="textright">3,969,000</div></td>
                <td><div class="textright">34,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">430i Convertible M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">4,229,000</div></td>
                <td><div class="textright">4,259,000</div></td>
                <td><div class="textright">36,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">520d M Sport</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">3,499,000</div></td>
                <td><div class="textright">3,539,000</div></td>
                <td><div class="textright">30,999</div></td>
            </tr>
            <tr>
                <th scope="row">530e Elite</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,959,000</div></td>
                <td><div class="textright">2,999,000</div></td>
                <td><div class="textright">25,999</div></td>
            </tr>
            <tr>
                <th scope="row">530e M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">3,699,000</div></td>
                <td><div class="textright">3,739,000</div></td>
                <td><div class="textright">32,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">530i Touring M Sport</th>
                <td><div class="textright">4,499,000</div></td>
                <td><div class="textright">4,539,000</div></td>
                <td><div class="textright">48,200</div></td>
            </tr> -->
            <tr>
                <th scope="row">630i GT M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">4,359,000</div></td>
                <td><div class="textright">4,399,000</div></td>
                <td><div class="textright">37,999</div></td>
            </tr>
            <tr>
                <th scope="row">730Ld sDrive M Sport</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">6,099,000</div></td>
                <td><div class="textright">6,139,000</div></td>
                <td><div class="textright">52,999</div></td>
            </tr>
            <tr>
                <th scope="row">745Le xDrive M Sport</th>
                <td><div class="textright">2,998</div></td>
                <td><div class="textright">6,399,000</div></td>
                <td><div class="textright">6,439,000</div></td>
                <td><div class="textright">55,999</div></td>
            </tr>
            <tr>
                <th scope="row">M760Li xDrive</th>
                <td><div class="textright">6,592</div></td>
                <td><div class="textright">13,499,000</div></td>
                <td><div class="textright">13,539,000</div></td>
                <td><div class="textright">115,999</div></td>
            </tr>
            <tr>
                <th scope="row">M850i xDrive Coupé</th>
                <td><div class="textright">4,395</div></td>
                <td><div class="textright">12,959,000</div></td>
                <td><div class="textright">12,999,000</div></td>
                <td><div class="textright">110,999</div></td>
            </tr>
            <tr>
                <th scope="row">M850i xDrive Convertible</th>
                <td><div class="textright">4,395</div></td>
                <td><div class="textright">13,459,000</div></td>
                <td><div class="textright">13,499,000</div></td>
                <td><div class="textright">115,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">X1 sDrive18i (Iconic)</th>
                <td><div class="textright">1,499</div></td>
                <td><div class="textright">1,969,000</div></td>
                <td><div class="textright">1,999,000</div></td>
                <td><div class="textright">17,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">X1 sDrive20d xLine</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">2,329,000</div></td>
                <td><div class="textright">2,359,000</div></td>
                <td><div class="textright">20,999</div></td>
            </tr>
            <tr>
                <th scope="row">X1 sDrive20d M Sport</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">2,529,000</div></td>
                <td><div class="textright">2,559,000</div></td>
                <td><div class="textright">22,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">X2 sDrive20i M Sport X</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">2,969,000</div></td>
                <td><div class="textright">2,999,000</div></td>
                <td><div class="textright">25,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">X3 xDrive20d M Sport</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">3,629,000</div></td>
                <td><div class="textright">3,659,000</div></td>
                <td><div class="textright">31,999</div></td>
            </tr>
            <tr>
                <th scope="row">X3 xDrive20e xLine</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">3,269,000</div></td>
                <td><div class="textright">3,299,000</div></td>
                <td><div class="textright">28,999</div></td>
            </tr>
            
            <tr>
                <th scope="row">X3 xDrive30e M Sport (Pro)</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">3,769,000</div></td>
                <td><div class="textright">3,799,000</div></td>
                <td><div class="textright">32,999</div></td>
            </tr>
            <tr>
                <th scope="row">X3 xDrive20d M Sport (M Performance Edition)</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">3,829,000</div></td>
                <td><div class="textright">3,859,000</div></td>
                <td><div class="textright">33,999</div></td>
            </tr>
            <tr>
                <th scope="row">X4 xDrive20d M Sport X</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">3,969,000</div></td>
                <td><div class="textright">3,999,000</div></td>
                <td><div class="textright">34,999</div></td>
            </tr>
            <tr>
                <th scope="row">X4 xDrive20d M Sport X (M Performance Edition)</th>
                <td><div class="textright">1,995</div></td>
                <td><div class="textright">4,169,000</div></td>
                <td><div class="textright">4,199,000</div></td>
                <td><div class="textright">35,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">X5 xDrive30d xLine</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">4,459,000</div></td>
                <td><div class="textright">4,499,000</div></td>
                <td><div class="textright">38,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">X5 xDrive30d M Sport</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">4,659,000</div></td>
                <td><div class="textright">4,699,000</div></td>
                <td><div class="textright">40,999</div></td>
            </tr>
            
            <tr>
                <th scope="row">X5 xDrive45e M Sport</th>
                <td><div class="textright">2,998</div></td>
                <td><div class="textright">4,959,000</div></td>
                <td><div class="textright">4,999,000</div></td>
                <td><div class="textright">42,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">X5 xDrive40e M Sport</th>
                <td><div class="textright">4,699,000</div></td>
                <td><div class="textright">4,739,000</div></td>
                <td><div class="textright">50,300</div></td>
            </tr> -->
            <tr>
                <th scope="row">X6 xDrive30d M Sport</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">7,259,000</div></td>
                <td><div class="textright">7,299,000</div></td>
                <td><div class="textright">62,999</div></td>
            </tr>
            <tr>
                <th scope="row">X7 xDrive30d M Sport</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">5,959,000</div></td>
                <td><div class="textright">5,999,000</div></td>
                <td><div class="textright">51,999</div></td>
            </tr>
            <tr>
                <th scope="row">Z4 sDrive30i M Sport</th>
                <td><div class="textright">1,998</div></td>
                <td><div class="textright">3,969,000</div></td>
                <td><div class="textright">3,999,000</div></td>
                <td><div class="textright">34,999</div></td>
            </tr>
            <tr>
                <th scope="row">Z4 M40i</th>
                <td><div class="textright">2,998</div></td>
                <td><div class="textright">4,969,000</div></td>
                <td><div class="textright">4,999,000</div></td>
                <td><div class="textright">42,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">i3s </th>
                <td><div class="textright">-</div></td>
                <td><div class="textright">3,700,000</div></td>
                <td><div class="textright">3,730,00</div></td>
                <td><div class="textright">32,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">iX xDrive50 Sport</th>
                <td><div class="textright">Electric</div></td>
                <td><div class="textright">5,959,000</div></td>
                <td><div class="textright">5,999,000</div></td>
                <td><div class="textright">51,999</div></td>
            </tr>
            <tr>
                <th scope="row">iX3 M Sport</th>
                <td><div class="textright">Electric</div></td>
                <td><div class="textright">3,359,000</div></td>
                <td><div class="textright">3,399,000</div></td>
                <td><div class="textright">29,999</div></td>
            </tr>
            <tr>
                <th scope="row">i8 Roadster</th>
                <td><div class="textright">1,499</div></td>
                <td><div class="textright">12,959,000</div></td>
                <td><div class="textright">12,999,000</div></td>
                <td><div class="textright">110,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">i8 (Pure Impulse)</th>
                <td><div class="textright">12,349,000</div></td>
                <td><div class="textright">12,389,000</div></td>
                <td><div class="textright">130,800</div></td>
            </tr> -->
            <!-- <tr>
                <th scope="row">M2 CS</th>
                <td><div class="textright">2,979</div></td>
                <td><div class="textright">6,959,000</div></td>
                <td><div class="textright">6,999,000</div></td>
                <td><div class="textright">59,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">M4 Coupé</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">7,959,000</div></td>
                <td><div class="textright">7,999,000</div></td>
                <td><div class="textright">68,999</div></td>
            </tr>
             <tr>
                <th scope="row">M4 Competition Coupé</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">9,959,000</div></td>
                <td><div class="textright">9,999,000</div></td>
                <td><div class="textright">85,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">M2  Competition</th>
                <td><div class="textright">2,979</div></td>
                <td><div class="textright">6,259,000</div></td>
                <td><div class="textright">6,299,000</div></td>
                <td><div class="textright">53,999</div></td>
            </tr> -->
            <!-- <tr>
                <th scope="row">M4 Coupé</th>
                <td><div class="textright">2,979</div></td>
                <td><div class="textright">8,399,000</div></td>
                <td><div class="textright">8,439,000</div></td>
                <td><div class="textright">72,999</div></td>
            </tr> -->
            <!-- <tr>
                <th scope="row">M4 CS</th>
                <td><div class="textright">2,979</div></td>
                <td><div class="textright">8,759,000</div></td>
                <td><div class="textright">8,799,000</div></td>
                <td><div class="textright">75,999</div></td>
            </tr> -->
            <!-- <tr>
                <th scope="row">M4 CS</th>
                <td><div class="textright">2,979</div></td>
                <td><div class="textright">11,399,000</div></td>
                <td><div class="textright">11,439,000</div></td>
                <td><div class="textright">97,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">M5</th>
                <td><div class="textright">4,395</div></td>
                <td><div class="textright">13,299,000</div></td>
                <td><div class="textright">13,339,000</div></td>
                <td><div class="textright">113,999</div></td>
            </tr>
            <!-- <tr>
                <th scope="row">M5 (Edition 35 Years)</th>
                <td><div class="textright">4,395</div></td>
                <td><div class="textright">13,959,000</div></td>
                <td><div class="textright">13,999,000</div></td>
                <td><div class="textright">119,999</div></td>
            </tr> -->
            <tr>
                <th scope="row">M8  Competition  Coupé</th>
                <td><div class="textright">4,395</div></td>
                <td><div class="textright">17,959,000</div></td>
                <td><div class="textright">17,999,000</div></td>
                <td><div class="textright">153,999</div></td>
            </tr>
            <tr>
                <th scope="row">X3 M</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">7,659,000</div></td>
                <td><div class="textright">7,699,000</div></td>
                <td><div class="textright">65,999</div></td>
            </tr>
            <tr>
                <th scope="row">X4 M</th>
                <td><div class="textright">2,993</div></td>
                <td><div class="textright">7,959,000</div></td>
                <td><div class="textright">7,999,000</div></td>
                <td><div class="textright">68,999</div></td>
            </tr>



            </tbody>
        </table>
    </div>

@endsection

@section('script')

@endsection
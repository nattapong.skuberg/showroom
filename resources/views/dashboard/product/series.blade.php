@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item"><a href="">Product</a></li>
                            <li class="breadcrumb-item active">Series</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Series</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="text-dark header-title m-t-0">Series Form</h4>
                    <br>
                    <form class="form-horizontal" role="form" method="post" action="{{ route('dashboard.series.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ (isset($temp))? $temp->id : ''  }}">
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Name</label>
                            <div class="col-10">
                                <input type="text" name="name" class="form-control" value="{{ (isset($temp))? $temp->name : ''  }}" placeholder="Series name" required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Cover</label>
                            <div class="col-10">
                                <input type="file" class="filestyle" name="cover" data-placeholder="Series cover" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                @if ($errors->has('cover'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cover') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Cover2</label>
                            <div class="col-10">
                                <input type="file" class="filestyle" name="cover2" data-placeholder="Series cover 2" id="filestyle-2" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                @if ($errors->has('cover2'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cover2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label"></label>
                            <div class="col-10">
                                <button type="submit" class="btn btn-success">Save</button>
                                <a href="{{ route('dashboard.series.index') }}" class="btn btn-warning">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card-box table-responsive">
                    <h4 class="text-dark header-title m-t-0">Series</h4>
                    <br>
                    <table id="datatable" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Cover</th>
                            <th>Cover 2</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($series as $item)
                            <tr>
                                <td>
                                    {{ $item->id }}
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>
                                    <img src="{{ asset($item->cover) }}" alt="" width="150px">
                                </td>
                                <td>
                                    <img src="{{ asset($item->cover2) }}" alt="" width="150px">
                                </td>
                                <td>
                                    <a href="{{ route('dashboard.series.edit', ['id'=> $item->id]) }}" class="btn btn-default">Edit</a>
                                    <a href="{{ route('dashboard.series.delete', ['id' => $item->id]) }}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div> <!-- end container -->
@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable();

        });
    </script>
@endsection

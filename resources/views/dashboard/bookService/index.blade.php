@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item active">Test Drive</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Book Service</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">

                    <ul class="nav nav-tabs navtab-bg nav-justified">
                       <select class="custom-select mt-2 mb-2" onchange="location = this.value;">
                           <option value="{{ route('dashboard.service.index.spec',['id'=>0]) }}" >
                               ทุกบริการ
                           </option>
                           @foreach($services as $service)
                               <option value="{{ route('dashboard.service.index.spec',['id'=>$service->id]) }}" {{ $id==$service->id?'selected':'' }}>
                                   {{ $service->name }}
                               </option>
                           @endforeach
                       </select>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="home1">
                            <table id="datatable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($books as $book)
                                    <tr>
                                        <td>
                                            {{ $book->first_name." ".$book->last_name }}
                                        </td>
                                        <td>
                                            {{ $book->created_at }}
                                        </td>
                                        <td>
                                            <a href="{{ route('dashboard.service.detail',['id'=>$book->id]) }}" class="btn btn-info waves-effect waves-light">detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div> <!-- end container -->
@endsection

@section('script')

@endsection

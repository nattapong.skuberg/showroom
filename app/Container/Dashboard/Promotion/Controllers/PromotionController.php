<?php

namespace App\Container\Dashboard\Promotion\Controllers;

use App\Container\Dashboard\Post\Models\Post;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        $data['posts'] = Post::where('type_id', 2)->get();

        return view('dashboard.promotion.index', $data);
    }

    public function create()
    {
        $data['type'] = 'Promotion';
        $data['mode'] = 1;
        $data['action'] = route('dashboard.promotion.store');
        $data['cancel'] = route('dashboard.promotion.index');


        return view('dashboard.post.index', $data);
    }

    public function edit($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 2)->first();

        if ($post == null){
            return redirect()->back()->withInput('Promotion not found.');
        }

        $data['post'] = $post;
        $data['type'] = 'Promotion';
        $data['mode'] = 2;
        $data['action'] = route('dashboard.promotion.update');
        $data['cancel'] = route('dashboard.promotion.index');

        return view('dashboard.post.index', $data);
    }

    public function destroy($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 2)->first();

        if ($post == null){
            return redirect()->back();
        }

        $post->delete();

        return redirect()->route('dashboard.promotion.index');
    }

    public function store(Request $request)
    {
        $validator = $this->postService->validatorCreate($request->all(), 2);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 2);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'Promotion create fail.']);
        }

        return redirect()->route('dashboard.promotion.index')->withInput(['msg' => 'Create promotion success.']);
    }

    public function update(Request $request)
    {
        $validator = $this->postService->validatorUpdate($request->all(), 2);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 2);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'Promotion update fail.']);
        }

        return redirect()->route('dashboard.promotion.index')->withInput(['msg' => 'Update promotion success.']);
    }

}
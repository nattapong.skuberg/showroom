@extends('layouts.app')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <section class="content">
        <div class="container">
            <h3>PROMOTION</h3>
            <div class="row">
                @foreach($posts as $item)
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="{{ asset($item->cover_image) }}">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->title }}</h5>
                                <hr>
                                <p class="card-text">
                                    {!! preg_replace('/\s+?(\S+)?$/', '', substr(strip_tags($item->detail), 0, 100)); !!}
                                </p>
                                <a href="{{ route('promotion.detail', ['id' => $item->id]) }}">> เพิ่มเติม</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $posts->links() }}
            <hr class="hr-end">
        </div>
    </section>
@endsection

@section('script')

@endsection
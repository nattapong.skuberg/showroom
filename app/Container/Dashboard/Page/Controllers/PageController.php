<?php

namespace App\Container\Dashboard\Page\Controllers;

use App\Container\Dashboard\Post\Models\Post;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        $data['posts'] = Post::where('type_id', 4)->get();

        return view('dashboard.page.index', $data);
    }

    public function create()
    {
        $data['type'] = 'Page';
        $data['mode'] = 1;
        $data['action'] = route('dashboard.page.store');
        $data['cancel'] = route('dashboard.page.index');


        return view('dashboard.post.index', $data);
    }

    public function edit($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 4)->first();

        if ($post == null){
            return redirect()->back()->withInput('Page not found.');
        }

        $data['post'] = $post;
        $data['type'] = 'Page';
        $data['mode'] = 2;
        $data['action'] = route('dashboard.page.update');
        $data['cancel'] = route('dashboard.page.index');

        return view('dashboard.post.index', $data);
    }

    public function destroy($id = 0)
    {
        $post = Post::where('id',$id)->where('type_id', 4)->first();

        if ($post == null){
            return redirect()->back();
        }

        $post->delete();

        return redirect()->route('dashboard.page.index');
    }

    public function store(Request $request)
    {
        $validator = $this->postService->validatorCreate($request->all(), 4);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 4);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'Page create fail.']);
        }

        return redirect()->route('dashboard.page.index')->withInput(['msg' => 'Create page success.']);
    }

    public function update(Request $request)
    {
        $validator = $this->postService->validatorUpdate($request->all(), 4);

        if ($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $post = $this->postService->savePost($request->all(), 4);

        if ($post == null){
            return redirect()->back()->withInput(['msg' => 'Page update fail.']);
        }

        return redirect()->route('dashboard.page.index')->withInput(['msg' => 'Update page success.']);
    }

}
<?php

namespace App\Container\Dashboard\TestDrive\Models;

use Illuminate\Database\Eloquent\Model;

class SubDistrict extends Model
{
    public $timestamps = false;
}

@extends('layouts.app')
@section('harder')
    <style>
        .time-confirm {
            width: 295px;
            border: 1px solid #ccc;
            margin: 0 auto;
            float: none;
            cursor: pointer;
            text-align: center;
        }
        .step-box {
            display: none;
        }
        .btn {
            border: 1px solid gray;
        }
        .form-step {
            margin-top: 1rem;
            margin-bottom: 1rem;
        }
        input, select {
            border-radius: 0px !important;
        }
        button {
            border-radius: 0px !important;
            border: solid 1px rgba(0,0,0,0);
            background-color: #1c69d5 !important;
        }
        .line-form {
            border-color: #e5e5e5;
            width: 100%;
        }
        .content-agreement {
            text-align: justify;
            text-indent: 3rem;
        }
    </style>
@endsection
@section('content')

    <div class="container">
        <form action="{{ route('testdrive') }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-12 form-step">
                <h3>ทดลองขับ</h3>

                <p>กรุณากรอกข้อมูลในแบบฟอร์มให้ครบถ้วน และกดยืนยัน ทางตัวแทนจำหน่ายที่คุณต้องการจะติดต่อกลับ เพื่อการทดลองขับต่อไป</p>
                <hr>
                <div class="row col-md-12">
                    <div class="form-group col-md-6">
                        <label for="name">ชื่อ</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="name" value="{{ isset($_GET['name'])?$_GET['name']:'' }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="lastname">นามสกุล</label>
                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="lastname" value="{{ isset($_GET['last_name'])?$_GET['last_name']:'' }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">อีเมลล์</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="email" value="{{ isset($_GET['email'])?$_GET['email']:'' }}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone">เบอร์โทรศัพท์มือถือ</label>
                        <input type="text" name="phone" class="form-control" id="phone" placeholder="phone" value="{{ isset($_GET['tel'])?$_GET['tel']:'' }}" required>
                    </div>
                    {{--<div class="form-group col-md-12">--}}
                        {{--<label for="address">ที่อยู่</label>--}}
                        {{--<input type="text" name="address" class="form-control" id="address" placeholder="address">--}}
                    {{--</div>--}}
                    {{--<div class="form-group col-md-6">--}}
                        {{--<label for="province">จังหวัด</label>--}}
                        {{--<select id="province" class="form-control" name="province">--}}
                            {{--<option value="0">กรุณาเลือกจังหวัด</option>--}}
                            {{--@foreach($provinces as $province)--}}
                            {{--<option value="{{ $province->id }}">{{ $province->name }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="form-group col-md-6">--}}
                        {{--<label for="district">อำเภอ / เขต</label>--}}
                        {{--<select name="district" class="form-control" id="district">--}}
                            {{--<option value="0">กรุณาเลือกอำเภอ</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="form-group col-md-6">--}}
                        {{--<label for="sub-district">ตำบล / แขวง</label>--}}
                        {{--<select name="sub-district" class="form-control" id="sub-district">--}}
                            {{--<option value="0">กรุณาเลือกตำบล</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="form-group col-md-6">--}}
                        {{--<label for="zip-code">รหัสไปรษณีย์</label>--}}
                        {{--<input type="text" name="zip-code" class="form-control" id="zip-code" placeholder="zip code">--}}
                    {{--</div>--}}
                    <div class="form-group col-md-6">
                        <label for="series">series</label>
                        <select id="series" name="series" class="form-control" required>
                            @foreach($series as $serie)
                                <option value="{{ $serie->id }}" {{ isset($_GET['model'])? ( $_GET['model'] == $serie->id? 'selected':''):'' }}>{{ $serie->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--<div class="form-group col-md-6">--}}
                        {{--<label for="drive-time">เวลาทดลองขับ</label>--}}
                        {{--<input type="datetime-local" name="drive-time" class="form-control" id="drive-time" placeholder="Drive time" required>--}}
                    {{--</div>--}}
                    <div class="form-group col-md-6">
                        <label for="drive-time">สาขา</label>
                        <select type="datetime-local" name="branch" class="form-control" id="branch" required>
                            <option value="" disabled="true" selected>กรุณาเลือกสาขา</option>
                            @foreach($branch as $b)
                            <option value="{{ $b }}">{{ $b }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">
                        <p>ท่านสนใจบริการทางการเงินของ BMW ด้วยหรือไม่ </p>
                    </div>
                    <div class="col-md-12">
                        <label><input type="radio" name="optradio" value="0"> ใช่ </label>
                        <label><input type="radio" name="optradio" value="1"> ไม่ </label>
                    </div>
                    <hr class="line-form">
                    <div class="content-agreement">
                        <p>ข้าพเจ้าขอให้ความยินยอมในการจัดเก็บ ใช้ ส่งต่อ โอน หรือ เปิดเผยข้อมูลส่วนบุคคลของข้าพเจากบริษัท บีเอ็มดับเบิล ยู (ประเทศไทย) จำกัด กลุ่มบรษัท บีเอ็มดับเบิลยู (ประเทศไทย) จำกัด คู่ค้า บุคคล/นิติบุคคลที่บรษัท บีเอ็มดับเบิลยู (ประเทศไทย) จำกัด เห็นสมควรและหน่วยราชการใด ๆ ตามที่กฎหมายกำหนดเพื่อวัตถุประสงค์ทางการค้าและการ ดำเนินการทางกฎหมาย รวมถึงแต่ไม่จำกดเพียงการดำเนินการเกี่ยวกับการรับประกันและข้อพิพาทใดๆ กิจกรรมหลังการขายเช่น บริการเคลื่อนย้าย (การช่วยเหลือจากริมถนน) แพ็คเกจต้อนรับ และกิจกรรมทางการขายและการตลาด เช่น จดหมายข่าว และกิจกรรมอื่นๆ และกิจกรรมลูกค้าสัมพันธ์ เช่น การสำรวจดัชนีความพึงพอใจของลูกค้า การตลาดแบบลูกค้าสัมพันธ์ และอื่นๆ รวมถึงข้าพเจ้าตกลงที่จะได้รับการติดต่อโดยไปรษณีย์ จดหมายอิเล็กทรอนิกส์ โทรศัพท์ และ ข้อความเกี่ยวกับข่าวสารของกลุ่มรถยนต์บีเอ็มดับเบิลยู สินค้าและบริการอื่น (ของบริษัท บีเอ็มดับเบิลยู (ประเทศไทย) จำกัด กลุ่มบริษัท บีเอ็มดับเบิลยู (ประเทศไทย) จำกัด และคู่ค้าที่ได้รับการคัดเลือกอย่างระมัดระวัง) ซึ่งอาจเป็นประโยชน์ กับข้าพเจ้า บริษัท บีเอ็มดับเบิลยู (ประเทศไทย) จำกัด อาจขอความช่วยเหลือจากข้าพเจ้าเป็นครัังคราวเกี่ยวกับการทำวิจัยทางการตลาด เพื่อช่วยพัฒนาบริการให้กับลูกค้า </p>
                    </div>
                    <hr class="line-form">
                </div>
                <button class="btn btn-success" type="submit">Submit</button>
            </div>
        </form>
    </div>

@endsection
@section('custom-script')
    <script>
        <?php
            if($errors->any()) {
            ?>
            swal('Error',
        'กรุณาใส่ข้อความให้ครบทุกช่อง',
        'error');
        <?php
            }
            ?>
        $( "#province" ).change(function() {
            $.ajax({
                method: "POST",
                url: "{{ route('district.all') }}",
                data: { id: $('#province').val() }
            })
                .done(function( resp ) {
                    $('#district').html('<option value="0">กรุณาเลือกอำเภอ</option>');
                    console.log(resp);
                    for( var i = 0 ; i < resp.district.length ; i++){
                        $('#district').append('<option value="' + resp.district[i].id + '">' + resp.district[i].name + '</option>');
                    }
                });
        });
        $( "#district" ).change(function() {
            $.ajax({
                method: "POST",
                url: "{{ route('subdistrict.all') }}",
                data: { id: $('#district').val() }
            })
                .done(function( resp ) {
                    $('#sub-district').html('<option value="0">กรุณาเลือกตำบล</option>');
                    console.log(resp);
                    for( var i = 0 ; i < resp.subdistrict.length ; i++){
                        $('#sub-district').append('<option value="' + resp.subdistrict[i].id + '">' + resp.subdistrict[i].name + '</option>');
                    }
                });
        });
    </script>
@endsection
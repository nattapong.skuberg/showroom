<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            'เปลี่ยนน้ำมันเครื่องและไส้กรองน้ำมันเครื่อง',
            'เปลี่ยนน้ำมันเบรก',
            'เช็คเปลี่ยนจานเบรก/ผ้าเบรกหน้า',
            'เช็คเปลี่ยนจานเบรก/ผ้าเบรกหลัง',
            'เช็คเปลี่ยนใบปัดน้ำฝน',
            'เช็คเปลี่ยนไส้กรองอากาศและหัวเทียน',
            'เช็คเปลี่ยนแผ่นกรองระบบปรับอากาศ',
            'สลับยาง เช็คศูนย์ ถ่วงล้อ',
            'เปลี่ยนโช๊คอัพด้านหน้า',
            'เปลี่ยนโช๊คอัพด้านหลัง',
        ];

        foreach ($services as $item){
            $service = new \App\Container\Dashboard\BookService\Models\Service();
            $service->name = $item;
            $service->save();
        }
    }
}

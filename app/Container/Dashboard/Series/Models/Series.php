<?php

namespace App\Container\Dashboard\Series\Models;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    public $timestamps = false;
}

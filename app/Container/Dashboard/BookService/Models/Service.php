<?php

namespace App\Container\Dashboard\BookService\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $timestamps = false;
}

@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->


        <div class="row">
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="md md-directions-car text-primary"></i>
                    <h2 class="m-0 text-dark counter font-600">50568</h2>
                    <div class="text-muted m-t-5">Test Drive</div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="fa fa-stethoscope text-info"></i>
                    <h2 class="m-0 text-dark counter font-600">18</h2>
                    <div class="text-muted m-t-5">Book Service</div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xl-4">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="md md-account-child text-custom"></i>
                    <h2 class="m-0 text-dark counter font-600">8564</h2>
                    <div class="text-muted m-t-5">Users</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="text-dark header-title m-t-0">Google Analytics</h4>
                    <div class="row">
                        <div class="col-md-12 col-xl-8">


                        </div>
                    </div>

                    <!-- end row -->

                </div>

            </div>

        </div>
        <!-- end row -->

    </div> <!-- end container -->
@endsection

@section('script')

@endsection

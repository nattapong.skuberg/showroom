@extends('layouts.admin')

@section('meta')

@endsection

@section('harder')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">BMW Performance</a></li>
                            <li class="breadcrumb-item"><a href="">Product</a></li>
                            <li class="breadcrumb-item active">Model</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Model</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-lg-12">
                <a href="{{ route('dashboard.model.create') }}" class="btn btn-default">New Model</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-vertical-env">
                    <ul class="nav tabs-vertical">
                        @foreach($series as $index => $item)
                            <li class="nav-item">
                                <a href="#v-{{ str_replace(' ','', $item->name) }}" data-toggle="tab" aria-expanded="true" class="nav-link {{ ($index == 0)? 'active' : '' }}">
                                    {{ $item->name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="tab-content" style="width: 100%;">
                        @foreach($series as $index => $item)
                            <div class="tab-pane  {{ ($index == 0)? 'active' : '' }}" id="v-{{ str_replace(' ','', $item->name) }}" aria-expanded="true" >
                                <div class="row">
                                    <div class="col-md-12 table-responsive">
                                        <h4>{{ $item->name }}</h4>
                                        <br>

                                        <table id="datatable-{{ str_replace(' ','', $item->name) }}" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Cover</th>
                                                <th>Tools</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($models as $model)
                                                @if($model->series_id == $item->id)
                                                    <tr>
                                                        <td>
                                                            {{ $model->title }}
                                                        </td>
                                                        <td>
                                                            <img src="{{ asset($model->cover_image) }}" alt="" width="150px">
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('dashboard.model.edit', ['id'=> $model->id]) }}" class="btn btn-default">Edit</a>
                                                            <a href="{{ route('dashboard.model.delete', ['id' => $model->id]) }}" class="btn btn-danger">Delete</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>


    </div> <!-- end container -->
@endsection

@section('script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            @foreach($series as $index => $item)
                $('#datatable-{{ str_replace(' ','', $item->name) }}').DataTable();
            @endforeach
        });
    </script>
@endsection

@extends('layouts.app')

@section('meta')

@endsection

@section('harder')

@endsection

@section('content')
<section class="content">
        <div class="container">
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW 2 Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/banner01.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 2 SERIES GRAN COUPÉ</p>
                                    <a href="../../assets/images/the-2.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW 3 Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/banner02.jpg"></p>
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 3 Series Sedan</p>
                                    <a href="../../assets/images/the-3.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>
                                    </div>

                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/cover2.png"></p>
                                    </div>

                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 3 SERIES GRAN SEDAN</p>
                                    <a href="../../assets/images/the-3.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>
                                    </div>
                                   
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW 4 Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/cover4scp.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 4 SERIES COUPÉ</p>
                                    <a href="../../assets/images/the-4.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>
                                    </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW 5 Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/cover5ss.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 5 Series Sedan</p>
                                    <a href="../../assets/images/the-5.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW 6 Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/banner04.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 6 Series Gran Turismo</p>
                                    <a href="../../assets/images/the-6.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW 7 Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/cover4sd.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW 7 Series Sedan</p>
                                    <a href="../../assets/images/the-7.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW X Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/coverx1.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW X1</p>
                                    <a href="../../assets/images/the-x1.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/coverx3.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW X3</p>
                                    <a href="../../assets/images/the-x3.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/coverx4.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW X4</p>
                                    <a href="../../assets/images/the-x4.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/coverx5.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW X5</p>
                                    <a href="../../assets/images/the-x5.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/coverx7.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW X7</p>
                                    <a href="../../assets/images/the-x7.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                                   
                                  
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <h3 style="color:#666">BMW Z Series</h3>
                        <hr>
                        </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p><img class="d-block w-100" src="../../assets/images/cover7.jpg"></p>
                                       
                                    </div>
                                    <div class="col-12 col-md-6 bro">
                                    <p>BMW Z4 Roadster</p>
                                    <a href="../../assets/images/the-z4.pdf" class="btn btn-slide"> <i class="fas fa-file-download"></i>โบรชัวร์</a>

                                    </div>
                    </div>
        </div>
</section>
<!-- 
    <section class="content">
        <div class="container">
        <div class="row">
        <h2>บีเอ็มดับเบิลยู ซีรีส์ 2<h2>
        <div class="col-md-6">
<p><img class="d-block w-100" src="../../assets/images/banner2102.jpg"></p>
</div>

<div class="col-md-6">
<p>บีเอ็มดับเบิลยู ซีรีส์ 2 แกรน คูเป้</p>
<i class="fas fa-download"></i> <a href="../../assets/images/the-2.pdf" class="btn btn-slide">โบรชัวร์</a>
</div>
</div>
        </div>
    </section> -->
@endsection

@section('script')

@endsection

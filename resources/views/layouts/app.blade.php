<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Performance Motors Thailand|เพอร์ฟอร์แมนซ์ มอเตอร์ (ประเทศไทย) จำหน่ายบีเอ็มดับเบิลยู อย่างเป็นทางการ</title>
    <meta name="description" content="ให้บริการ การขาย การบริการและความพึงพอใจลูกค้าคือปัจจัยที่ผู้จำหน่ายอย่างเป็นทางการของบีเอ็มดับเบิลยูให้ความสำคัญเป็นพิเศษและเป็นสิ่งที่เราภูมิใจมาเป็นระยะเวลายาวนานกับคำมั่นสัญญญาที่มีต่อการบริการลูกค้า และยิ่งไปกว่านั้นเพื่อเป็นการให้บริการที่สามารถตอบสนองความต้องการ
เรามีบริการครบวงจรตามมาตรฐานบีเอ็มดับเบิลยู">
    @yield('meta')
    <link rel="icon" href="{{ asset('assets/images/bmwlogofav.png') }}" sizes="32x32">
    <link rel="icon" href="{{ asset('assets/images/bmwlogofav2.png') }}" sizes="192x192">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.21.1/sweetalert2.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    @yield('harder')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    
    @yield('custom-style')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71262257-4"></script>

 <!-- Facebook Pixel Code -->
 <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '517527009261477');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=517527009261477&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-71262257-4');
</script>
</head>

<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="136422966528072"
     theme_color="#0084ff"
     logged_in_greeting="สวัสดีครับ!! Performance motor thailand สอบถามได้ครับ"
     logged_out_greeting="สวัสดีครับ!! Performance motor thailand สอบถามได้ครับ">
</div>
    <div class="container borderbottom">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand d-none d-md-block" href="/">Performance Motors (Thailand)</a>
            <a class="navbar-brand d-block d-md-none" href="/">
                <img src="{{ asset('/assets/images/logo-bmw2.png') }}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <span class="line-navbar d-none d-md-none d-lg-block"></span>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav menu-top">
                    <!-- <li class="nav-item active">
                        <a class="nav-link" href="{{ route('promotion.list') }}">Special Deal</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.all') }}">All Models</a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.brochure') }}">Brochure</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.bmwperformancemotors.com">Request For Test Drive</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('testdrive') }}">Request For Test Drive</a>
                    </li> -->
                </ul>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://en.bmw-performancemotors.shop/">BMW Lifestyle Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('post.all') }}">News and Event</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('contact') }}">Contact us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('specialDeal.all')}}">Promotion</a>
                    </li>
                </ul>
                <div class="my-2 my-lg-0">
                <a class="nav-link" href="/"><div class="img-show d-none d-md-none d-lg-block">
                        <img src="{{ asset('assets/images/logo-bmw2.png') }}">
                    </div></a>
                </div>
            </div>
        </nav>
    </div>

     @yield('content')

    <section class="footer">
        <div class="container">
            <hr class="hr-end">
            <div class="row">
                
                 
                <div class="col-md-4">
                    <h4>Performance Motors (Thailand)</h4>
                        <p>Ramintra Tel. <a href="tel:02-900-5700">02-900-5700</a><br>
                          Charansanitwong Tel. <a href="tel:02-880-0999">02-880-0999</a><br>
                          Don Mueang Tel. <a href="tel:02-561-0999">02-561-0999</a>
                          </p>
                </div>
                <div class="col-md-5">
                    <p class="title" style=" padding-top: 14px;">
                        <b>Copyright &copy; 2015-2021 BMW Performance. All rights reserved.</b>
                    </p>
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="https://en.bmw-performancemotors.shop/">BMW Lifestyle</a>
                        </li>
                        <li>
                            <a href="{{ route('post.all') }}">News&Events</a>
                        </li>
                        <li>
                            <a href="{{ route('contact') }}">Contact us</a>
                        </li>

                    </ul>
                </div>
               <div class="col-md-3">
                    <a href="https://www.facebook.com/BMWPerformancemotors">
                        <i style="padding-top:15px; font-size: 4em;" class="fab fa-facebook-square fa-2x "></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCVFUIVWNBu4dKuZa6PhVufQ">
                        <i style="padding-top:15px; font-size: 4em;"  class="fab fa-youtube-square fa-2x "></i>
                    </a>
                    <a href="https://lin.ee/qyFgmv1">
                    <i style="padding-top:15px; font-size: 4em;"  class="fab fa-line fa-2x"></i>
                        <!-- <img style="margin-top: -35px;;" src="assets/images/line.png"> -->
                    </a>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.21.1/sweetalert2.all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.21.1/sweetalert2.all.min.js"></script>
    @yield('script')
    @yield('custom-script')

</body>
</html>

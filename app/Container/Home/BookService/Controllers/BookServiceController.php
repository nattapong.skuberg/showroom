<?php

namespace App\Container\Home\BookService\Controllers;

use App\Container\Dashboard\BookService\Models\BookService;
use App\Container\Dashboard\BookService\Models\Service;
use App\Http\Controllers\Controller;
use App\Services\MailService;
use Illuminate\Http\Request;

class BookServiceController extends Controller
{
    public function index()
    {
        $data['services'] = Service::all();
        return view('home.bookService.index',$data);
    }
    public function store(Request $request) {
//        dd($request->all());
        $book = new BookService();
        $book->first_name = $request->input('first_name');
        $book->last_name = $request->input('last_name');
        $book->email = $request->input('email');
        $book->phone = $request->input('phone');
        $book->model = $request->input('model');
        $book->uid = 0;
//        $book->uid = $request->input('uid');
        $date = date('Y-m-d', strtotime(str_replace('-', '/', $request->input('book_date'))));
        $book->book_time = $request->input('book_date');
        $services = Service::all();
        $sv = array();
        foreach ($services as $service) {
            if($request->input('service'.$service->id)) {
                array_push($sv,$service->id);
            }
        }
        $book->services = json_encode($sv);
        $book->save();
        $email = new MailService();
        $contact = '';
        $detail = 'New service request from '.$book->first_name.' '.$book->last_name.'<br>You can see detail at '.route('dashboard.testDrive.detail',['id'=>$book->id]);
        $email->sendMailToAdmin($book->first_name.' '.$book->last_name, $detail, now(), 'New service request from '.$book->first_name.' '.$book->last_name, $contact);

        $data['message'] = 'การจองบริการเสร็จสิ้น';
        return redirect(route('status', $data));
    }
}
<?php

namespace App\Container\Home\Address\Controllers;

use App\Container\Dashboard\TestDrive\Models\District;
use App\Container\Dashboard\TestDrive\Models\SubDistrict;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function getDistrict(Request $request)
    {
        $data['district'] = District::where('province_id',$request->input('id'))->get();
        return response()->json($data, 200);
    }
    public function getSubDistrict(Request $request)
    {
        $data['subdistrict'] = SubDistrict::where('district_id',$request->input('id'))->get();
        return response()->json($data, 200);
    }
}
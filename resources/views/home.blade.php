@extends('layouts.app')
@section('harder')
    <style>
        .test-drive {
            padding: 2rem;
            background: url( {{ asset('assets/images/boxformlead.jpg') }});
            background-size: cover;
        }

        .test-drive select {
            width: 100%;
            margin-bottom: 1rem;
        }

        .test-drive input {
            width: 100%;
            margin-bottom: 1rem;
        }

        .btn-submit {
            margin-right: auto;
            margin-left: auto;
            margin-top: 2rem;
            background-color: #1c69d5;
            border-radius: 0px;
            color: #ffffff;
            font-weight: bold;
        }
        .btn-submit:hover {
            background-color: rgba(28, 105, 213, 0.8);
        }

        .test-drive label {
            margin-bottom: 0px;
            color: black;
        }
        .form-test {
            padding: 1rem;
            background-color: rgba(255, 255, 255, 0.5);
        }
    </style>
@endsection

@section('content')
    <section class="slide">
        <div class="d-none d-sm-none d-md-block d-lg-block">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner">
                
                <div class="carousel-item slide-bg active"
                         style="background: url('assets/images/cover1sim.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                            <h1 style="color: #fff">           
                            Futures Made Better:
                            </h1>
                            <h3 style="color: #fff">  
                            Sime Darby at 110
                            </h3>
                            <a href="https://www.simedarby.com/futures-made-better" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item slide-bg"
                         style="background: url('assets/images/BMW-website-cover-05.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                        <h1 style="color: #fff">THEiX</h1>
                            <h3 style="color: #fff">
                            The first-ever BMW iX
                            </h3>
                            <a href="https://www.bmw-performancemotors.com/dashboard/product/product/68" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item slide-bg"
                         style="background: url('assets/images/cover2sim.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                        <h1 style="color: #fff">           
                        History
                            </h1>
                            <h3 style="color: #fff">  
                            Sime Darby
                            </h3>
                            <a href="https://www.simedarby.com/company/history" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item slide-bg"
                         style="background: url('assets/images/BMW-website-cover-07.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                        <h1 style="color: #fff">THEiX</h1>
                            <h3 style="color: #fff">
                            The first-ever BMW iX
                            </h3>
                            <a href="https://www.bmw-performancemotors.com/dashboard/product/product/68" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item slide-bg"
                         style="background: url('assets/images/BMW-website-cover-08.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                        <h1 style="color: #fff">THEiX</h1>
                            <h3 style="color: #fff">
                            The first-ever BMW iX
                            </h3>
                            <a href="https://www.bmw-performancemotors.com/dashboard/product/product/68" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <!-- <div class="carousel-item slide-bg"
                         style="background: url('assets/images/banner2104.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner" >
                            <h1 style="color: #fff">      
                            THEi4
                            </h1>
                            <h3 style="color: #fff">
                            The first-ever BMW i4
                            </h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div> -->
                    <!-- <div class="carousel-item slide-bg"
                         style="background: url('assets/images/banner2103.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                            <h1 style="color: #fff">THE 2</h1>
                            <h3 style="color: #fff">The new BMW X2 M Mesh Edition</h3>
                            <a href="{{ route('service.book') }}" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div> -->
                    <!-- <div class="carousel-item slide-bg"
                         style="background: url('assets/images/banner04.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                            <h1 style="color: #fff">             
                                    THE 6   
                            </h1>
                                                        <h3 style="color: #fff">       
                                    THE BMW 6  SERIES GRAN TURISMO    
                            </h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item slide-bg"
                         style="background: url('assets/images/banner05.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                            <h1 style="color: #fff">      
                                    THE M8  
                            </h1>
                            <h3 style="color: #fff">
                                    BMW 8 SERIES COUPÉ M AUTOMOBILES
                            </h3>
                            <a href="{{ route('service.book') }}" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item slide-bg"
                         style="background: url('assets/images/banner06.jpg') no-repeat; background-size: cover; background-position: 0px;">
                        <div class="container boxbanner">
                            <h1 style="color: #fff">            
                             THE X6
                            </h1>
                            <h3 style="color: #fff">
                                THE ALL-NEW BMW X6
                            </h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div> -->
                    
                </div>
            </div>
        </div>
        <div class="d-block d-sm-block d-md-none d-lg-none">
            <div id="carouselMobile" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselMobile" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselMobile" data-slide-to="1"></li>
                    <li data-target="#carouselMobile" data-slide-to="2"></li>
                    <li data-target="#carouselMobile" data-slide-to="3"></li>
                    <li data-target="#carouselMobile" data-slide-to="4"></li>
                    
                </ol>
                <div class="carousel-inner">
               
                <div class="carousel-item active">
                        <img class="d-block w-100" src="assets/images/cover1sim.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">Futures Made Better:</h1>
                            <h3 style="color: #000"> Sime Darby at 110</h3>
                            <a href="https://www.simedarby.com/futures-made-better" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="assets/images/BMW-website-cover-05.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">THEiX</h1>
                            <h3 style="color: #000">The first-ever BMW iX</h3>
                            <a href="https://www.bmw-performancemotors.com/dashboard/product/product/68" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="assets/images/cover2sim.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">History</h1>
                            <h3 style="color: #000"> Sime Darby</h3>
                            <a href="https://www.simedarby.com/company/history" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="assets/images/BMW-website-cover-07.jpg">
                        <div class="textleft20">
                        <h1 style="color: #000">THEiX</h1>
                            <h3 style="color: #000">The first-ever BMW iX</h3>
                            <a href="https://www.bmw-performancemotors.com/dashboard/product/product/68" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="assets/images/BMW-website-cover-08.jpg">
                        <div class="textleft20">
                        <h1 style="color: #000">THEiX</h1>
                            <h3 style="color: #000">The first-ever BMW iX</h3>
                            <a href="https://www.bmw-performancemotors.com/dashboard/product/product/68" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <!-- <div class="carousel-item">
                        <img class="d-block w-100" src="assets/images/banner2104.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">THEi4</h1>
                            <h3 style="color: #000">The first-ever BMW i4</h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div> -->
                    <!-- <div class="carousel-item">
                        <img class="d-block w-100" src="assets/images/banner2103.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">THE X2</h1>
                            <h3 style="color: #000">BMW 2 Series Gran Tourer</h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div> -->
                    <!-- <div class="carousel-item">
                        <img class="d-block w-100" src="assets/images/banner03.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">THE 6</h1>
                            <h3 style="color: #000">THE BMW 6  SERIES GRAN TURISMO</h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="assets/images/banner03.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">THE M8</h1>
                            <h3 style="color: #000">BMW 8 SERIES COUPÉ M AUTOMOBILES</h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="assets/images/banner03.jpg">
                        <div class="textleft20">
                            <h1 style="color: #000">THE X6</h1>
                            <h3 style="color: #000">THE ALL-NEW BMW X6</h3>
                            <a href="#" class="btn btn-slide">เพิ่มเติม</a>
                        </div>
                    </div> -->
                </div>
                <a class="carousel-control-prev" href="#carouselMobile" role="button" data-slide="prev">
                    <i class="fa fa-chevron-left fa-lg" aria-hidden="true"
                       style="background-color: gray;padding: 5px;"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselMobile" role="button" data-slide="next">
                    <i class="fa fa-chevron-right fa-lg" aria-hidden="true"
                       style="background-color: gray;padding: 5px;"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container">
            <h3 style="color:#666">All Models</h3>
            <div class="row">
                <div class="col-6 col-md-3 col-lg-2 models">
                    <a href="{{ route('product.all') }}">
                        <div class="box-models model-01">
                            <p class="text-model">
                                <b>ALL BMW Models</b>
                            </p>
                        </div>
                    </a>
                </div>
                @foreach($series as $item)
                    <div class="col-6 col-md-3 col-lg-2 models">
                        <a href="{{ route('product.series', ['id' => $item->id]) }}">
                            <div class="box-models model-02" style="background: url('{{ $item->cover }}');"
                                 onmouseover="this.style.background = 'url({{ $item->cover2 }}) no-repeat';"
                                 onmouseout="this.style.background = 'url({{ $item->cover }}) no-repeat';">
                                <p class="text-model">
                                    <b>{{ $item->name }}</b>
                                </p>
                            </div>
                        </a>
                    </div>
                @endforeach
                <div class="col-6 col-md-3 col-lg-2 models">
                    <a href="{{ route('product.brochure') }}">
                        <div class="box-models model-11">
                            <p class="text-model">
                                <b>Brochure</b>
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-12 col-md-4 models">
                    <a href="{{ route('post.all') }}">
                    <div class="parent" onclick="">
                        <div class="child bg-one">
                            <div class="titlelife">
                                <h4 style="color:#fff;">Promotion</h4>
                                <p style="color:#fff;"></p>
                            </div>
                        </div>
                    </div>
                    </a>
                    <a href="{{ route('pricelist')}}">
                    <div class="parent" onclick="">
                        <div class="child bg-two">
                            <div class="titlelife">
                                <h4 style="color:#fff;">Price List</h4>
                                <p style="color:#fff;">2021</p>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-12 col-md-8 models">
                    <a href="https://www.bmwperformancemotors.com/">
                    <div style="height: 340px" class="parent " onclick="">
                        <div class="child bg-three">
                            <div class="titlelife2">
                                <h4 style="color:#fff;">Test DRIVE</h4>
                                <p style="color:#fff;">Request</p>
                            </div>
                        </div>
                    </div>
                    </a>

                </div>
            </div>

            <div class="row" style="margin-top: 30px;">
                <div class="col-12 col-md-8">
                    <h3 style="color:#666">
                        News and Events
                        <span class="float-right">
                            <a href="{{ route('post.all') }}" style="font-size: 1rem;font-weight: normal;">View All</a>
                        </span>
                    </h3>
                    <hr>
                    <div id="carousel-news" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-news" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-news" data-slide-to="1"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    @foreach($news as $index => $item)
                                        @if($index <= 1 )
                                            <div class="col-12 col-md-6">
                                                <div class="card">
                                                    <img class="card-img-top" src="{{ asset($item->cover_image) }}"
                                                         class="img-fluid">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $item->title }}</h5>
                                                        <hr>
                                                        <p class="card-text">
                                                            {!! preg_replace('/\s+?(\S+)?$/', '', substr(strip_tags($item->detail), 0, 100)); !!}
                                                        </p>
                                                        <a href="{{ route('post.show', ['id' => $item->id]) }}">>
                                                            เพิ่มเติม</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    @foreach($news as $index => $item)
                                        @if($index > 1 )
                                            <div class="col-12 col-md-6">
                                                <div class="card">
                                                    <img class="card-img-top" src="{{ asset($item->cover_image) }}"
                                                         class="img-fluid">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $item->title }}</h5>
                                                        <hr>
                                                        <p class="card-text">
                                                            {!! preg_replace('/\s+?(\S+)?$/', '', substr(strip_tags($item->detail), 0, 100)); !!}
                                                        </p>
                                                        <a href="{{ route('post.show', ['id' => $item->id]) }}">>
                                                            เพิ่มเติม</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-4 responpaddingtop10">
                    <h3 style="color:#666">Special Offer</h3>
                    <hr>
                    <div class="test-drive">
                        <form id="contact-test" class="form-test">
                            <label for="model">Car Model*</label>
                            <select id="model" name="model" required>
                            <option disabled selected value="0">Select model</option>
                            @foreach($series as $s)
                            <option value="{{ $s->id }}">{{ $s->name }}</option>
                            @endforeach
                            </select>
                            <label for="name">Name*</label>
                            <input type="text" id="name" name="name" required>
                            <label for="email">Email*</label>
                            <input type="email" id="email" name="email" required>
                            <label for="tel">Tel*</label>
                            <input type="text" id="tel" name="tel" required>
                            <button type="submit" class="btn btn-submit d-flex justify-content-center"><span id="btn_submit">CONTACT SALES</span></button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('script')
    <script>
        @if((session()->has('message')))
                swal('success','{{ session()->get('message') }}','success');
        @endif
        $('#contact-test').submit(function (event) {
            $('#btn_submit').html('CONTACT SALES <i class="fa fa-circle-o-notch fa-spin"></i>');
            $.ajax({
                method: "POST",
                url: "{{ route('contact.test') }}",
                data: $('#contact-test').serialize()
            })
                .done(function (resp) {
                    console.log(resp);
                    $('#btn_submit').html('CONTACT SALES');
                    swal(
                        'Success',
                        'กรุณาการติดต่อกลับจากเจ้าหน้าที่',
                        'success'
                    )
                })
                .fail(function (err) {
                    $('#btn_submit').html('CONTACT SALES');
                    swal(
                        'Error',
                        'เกิดข้อผิดพลาดในการส่งข้อมูล',
                        'error'
                    )
                });
            return false;

        });
    </script>
@endsection